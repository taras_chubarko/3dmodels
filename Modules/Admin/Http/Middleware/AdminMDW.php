<?php

namespace Modules\Admin\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AdminMDW
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (\Sentinel::guest())
        {
            return redirect()->guest('/');
        }
        else
        {
            $user = \Sentinel::getUser();
            
            if($user->inRole('admin') || $user->hasAccess(['admin']))
            {
                return $next($request);
            }
            
            return redirect('/');
        }
    }
}
