<?php

namespace Modules\Data\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Site\Entities\CityRegion;
use Modules\Site\Entities\TariffRegion;
use Ixudra\Curl\Facades\Curl;

class DataController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        
        
        
    }
    /* public function fail
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function fail(Request $request)
    {
        \Log::info($request->all());
        dd($request->all());
    }
    /* public function success
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function success(Request $request)
    {
        \Log::info($request->all());
        return redirect('/oplata')->with('success', 'Оплата прошла успешно!');
    }
    /* public function check
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function check(Request $request)
    {
        \Log::useFiles(storage_path().'/yandexKassa.log');
        \Log::info($request->all());
        
        $kassa = config('app.yandexKassa');
        $configs = $kassa[$kassa['current']];
        $ShopPassword = $kassa['ShopPassword'];
        
        $hash = md5($request->action.';'.$request->orderSumAmount.';'.$request->orderSumCurrencyPaycash.';'.$request->orderSumBankPaycash.';'.$configs['shopId'].';'.$request->invoiceId.';'.$request->customerNumber.';'.$ShopPassword);		
	if (strtolower($hash) != strtolower($request->md5)){
	    $code = 1;
	}
	else {
	    $code = 0;
	}
	$content = '<?xml version="1.0" encoding="UTF-8"?>';
	$content .= '<checkOrderResponse performedDatetime="'. $request->requestDatetime .'" code="'.$code.'"'. ' invoiceId="'.$request->invoiceId .'" shopId="'. $configs['shopId'] .'"/>';
        
        return response($content, 200)
            ->header('Content-Type', 'application/xml', 'charset=utf-8');
    }
    /* public function aviso
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function aviso(Request $request)
    {
        \Log::useFiles(storage_path().'/yandexKassa.log');
        \Log::info($request->all());
        $kassa = config('app.yandexKassa');
        $configs = $kassa[$kassa['current']];
        $ShopPassword = $kassa['ShopPassword'];
        
        $hash = md5($request->action.';'.$request->orderSumAmount.';'.$request->orderSumCurrencyPaycash.';'.$request->orderSumBankPaycash.';'.$configs['shopId'].';'.$request->invoiceId.';'.$request->customerNumber.';'.$ShopPassword);		
	if (strtolower($hash) != strtolower($request->md5)){ 
	    $code = 1;
	}
	else {
            $code = 0;
            //$curl = Curl::to('https://api.vipabonent.ru/v1/number/add/yandex/invoice/'.$request->invoiceId.'/'.$request->cdd_pan_mask.'/')
            $customerNumber = str_replace('+7 ', '', $request->customerNumber);
            $curl =  Curl::to('https://api.vipabonent.ru/v1/number/add/payment/'.$customerNumber.'/'.$request->orderSumAmount.'/')
            //->withHeader('Authorization:'.config('app.api_key'))
            ->withHeader('Authorization: eae787e9-67d8-42c2-a46a-19a34d47256b')
            ->post();
            \Log::info($curl);
	}
	$content = '<?xml version="1.0" encoding="UTF-8"?>';
	$content .= '<paymentAvisoResponse performedDatetime="'. $request->requestDatetime .'" code="'.$code.'" invoiceId="'. $request->invoiceId .'" shopId="'. $configs['shopId'] .'"/>';
        
        return response($content, 200)
            ->header('Content-Type', 'application/xml', 'charset=utf-8');
    }
    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('data::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('data::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('data::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}
