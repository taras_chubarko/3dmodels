<?php namespace Modules\Filem\Facades;

use Illuminate\Support\Facades\Facade;

class Filem extends Facade
{

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'Modules\Filem\Repositories\FilemRepository';
    }
}