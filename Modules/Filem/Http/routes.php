<?php

Route::group(['middleware' => 'web', 'prefix' => 'filem', 'namespace' => 'Modules\Filem\Http\Controllers'], function()
{
	Route::post('upload/image', 'FilemController@upload_image');
	Route::post('upload/images', 'FilemController@upload_images');
	
	Route::get('image/{uri}/{size}/{name}', [
	    'as' 	=> 'filem.image',
	    'uses' 	=> 'FilemController@get_image'
	]);
	
	Route::post('destroy/{id}', [
	    'as' 	=> 'filem.destroy.id',
	    'uses' 	=> 'FilemController@destroy'
	]);
	
	Route::get('{uri}/{size}/{name}', [
	    'as' 	=> 'filem.filem',
	    'uses' 	=> 'FilemController@get_filem'
	]);

});

Route::get('avatar/{size?}/{user_id?}', [
	'as' 	=> 'filem.avatar',
	'uses' 	=> 'Modules\Filem\Http\Controllers\FilemController@get_avatar'
]);

Route::get('fake/image/{size?}/{num?}', [
	'as' 	=> 'fake.image',
	'uses' 	=> 'Modules\Filem\Http\Controllers\FilemController@fake_image'
]);


/*
 * Админка
 */
//Route::group(['middleware' => ['web', 'admin'], 'prefix' => 'admin', 'namespace' => 'Modules\Filem\Http\Controllers'], function()
//{
//	Route::resource('filem', 'FilemController');
//	Route::post('upload/images', 'FilemController@uploadImages');
//	Route::post('delete/images', 'FilemController@deleteImages');
//	Route::post('upload/filem', 'FilemController@uploadFilem');
//});