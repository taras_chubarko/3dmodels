<?php namespace Modules\Meta\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateMetaReq extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'entity'  	 => 'required',
			'robots' 	 => 'required',
			'title' 	 => 'required',
			'keywords' 	 => 'required',
			'description' 	 => 'required',
		];
	}
	
	protected function getValidatorInstance()
	{
		$validator = parent::getValidatorInstance();
	 
		$validator->setAttributeNames([
			'entity'  	 => '"Тип материала"',
			'robots' 	 => '"Индексация"',
			'title' 	 => '"Мета - title"',
			'keywords' 	 => '"Мета - keywords"',
			'description' 	 => '"Мета - description"',
		]);
	 
		return $validator;
	}

}
