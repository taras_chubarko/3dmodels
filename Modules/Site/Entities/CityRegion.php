<?php

namespace Modules\Site\Entities;

use Illuminate\Database\Eloquent\Model;

class CityRegion extends Model
{
    protected $table = 'cityregion';
    
    protected $fillable = [];
    
    protected $guarded = ['_token'];
    
    
    /* public function tree
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function tree()
    {
        
    }
    /* public function group
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function scopeGroups($query)
    {
        return $query->groupBy('vregion');
    }
}
