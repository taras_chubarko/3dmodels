<?php

namespace Modules\Site\Entities;

use Illuminate\Database\Eloquent\Model;

class TariffRegion extends Model
{
    protected $table = 'tariffregion';
    
    protected $fillable = [];
    
    protected $guarded = ['_token'];
}
