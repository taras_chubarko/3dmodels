<?php

namespace Modules\Site\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Site\Repositories\SiteRepository;
use Modules\Site\Entities\CityRegion;
use Ixudra\Curl\Facades\Curl;
use Faker\Factory as Faker;

class SiteController extends Controller
{
    /*
     *
     */
    protected $site;
    /*
     *
     */
    public function __construct(SiteRepository $site)
    {
        $this->site = $site;
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('site::layouts.master');
    }
    /* public function get_catalog
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function get_catalog()
    {
        $data = \TaxonomyTerm::tree(1);
	$data->load('nameLocale');
	
	$locales = ['en', 'ru', 'uk'];
	$names = [];
	foreach($locales as $loc)
	{
	    foreach($data as $item)
	    {
		$name = $item->nameLocale->where('pivot.locale', $loc)->first();
		$childs = $item->children;
		$childs->load('nameLocale');
		
		$childArr = [];
		if($childs->count())
		{
		    foreach($childs as $child)
		    {
			$nameChild = $child->nameLocale->where('pivot.locale', $loc)->first();
			$childArr[$child->id] = [
			    'id' => $child->id,
			    'name' => $nameChild->pivot->name,
			    'ico' => $child->ico,
			];
		    }
		}
		
		$items[$item->id] = [
		    'id' => $item->id,
		    'name' => $name->pivot->name,
		    'ico' => $name->ico,
		    'children' => $childArr,
		];
	    }
	    $names[$loc] = $items;
	}
        return response()->json($names, 200);
    }
    /* public function get_top_disainers
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function get_top_disainers()
    {
        $faker = Faker::create('uk_UA');
        $data = [];
        foreach(range(1, 3) as $i)
        {
            $images = [];
            foreach(range(1, 4) as $y)
            {
                $images[] = 'http://erundit.ru/image/cache/catalog/avatar/'.$faker->numberBetween(240, 310).'-200x200.jpg';
            }
            
            
            $data[] = [
                'user' => [
                    'name' => $faker->lastName.' '.$faker->firstName,
                    'avatar' => 'http://erundit.ru/image/cache/catalog/avatar/'.$faker->numberBetween(240, 310).'-200x200.jpg',
                ],
                'images' => $images,
                'flag' => $faker->numberBetween(0, 1000),
                'views' => $faker->numberBetween(0, 1000),
                'likes' => $faker->numberBetween(0, 1000),
                'follows' => $faker->numberBetween(0, 1000),
                'positiops' => $faker->numberBetween(0, 1000),
            ];
        }
        return response()->json($data, 200);
    }
    /* public function get_block_work
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function get_block_work()
    {
	$faker = Faker::create('uk_UA');
        $data = [];
	
	foreach(range(1, 3) as $i)
        {
	    $frilancers[] = [
		'name' => $faker->lastName.' '.$faker->firstName,
		'avatar' => 'http://erundit.ru/image/cache/catalog/avatar/'.$faker->numberBetween(240, 310).'-200x200.jpg',
		'models' => $faker->numberBetween(0, 1000),
		'reputation' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 100),
	    ];
	    
	    $works[] = [
		'user' => [
                    'name' => $faker->lastName.' '.$faker->firstName,
                    'avatar' => 'http://erundit.ru/image/cache/catalog/avatar/'.$faker->numberBetween(240, 310).'-200x200.jpg',
		    'reputation' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 100),
		    'last_visit' => $faker->numberBetween(1, 55),
                ],
		'work' => [
		    'title' => $faker->sentence(3),
		    'price' => [
			'min' => $faker->numberBetween(1, 100),
			'max' => $faker->numberBetween(100, 900),
		    ],
		],
	    ];
	}
	
	$data['frilancers'] = $frilancers;
	$data['works'] = $works;
	
	return response()->json($data, 200);
    }
    
}
