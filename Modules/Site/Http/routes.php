<?php
/*
 *
 */
Route::group(['middleware' => 'web', 'namespace' => 'Modules\Site\Http\Controllers'], function()
{
    Route::get('/{vue?}', 'SiteController@index')->where('vue', Site::setAliases());
});
/*
 *
 */
Route::group(['middleware' => 'api', 'prefix' => 'api/v1', 'namespace' => 'Modules\Site\Http\Controllers'], function()
{
    Route::post('get/catalog', [
        'as' => 'get.catalog',
        'uses' => 'SiteController@get_catalog'
    ]);
    
    Route::post('get/top/disainers', [
        'as' => 'get.top.disainers',
        'uses' => 'SiteController@get_top_disainers'
    ]);
    
    Route::post('get/block/work', [
        'as' => 'get.block.work',
        'uses' => 'SiteController@get_block_work'
    ]);
    
    
});









