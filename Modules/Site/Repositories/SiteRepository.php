<?php

namespace Modules\Site\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SiteRepository
 * @package namespace App\Repositories;
 */
interface SiteRepository extends RepositoryInterface
{
    /*
     *
     */
    public function getRegionsAdnCityes();
    /*
     *
     */
    public function setAliases();
    /*
     *
     */
    public function mb_ucfirst($str, $encoding, $lower_str_end);
    /*
     * 
     */
    public function getRC();
    /*
     * Тариф по регионам Частным клиентам - Sim для телефона :region - регион (msk,spb...)
     */
    public function getTariffRegionMainTelefon($region);
}
