<?php

namespace Modules\Site\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;
use Prettus\Repository\Criteria\RequestCriteria;
use Modules\Site\Repositories\SiteRepository;
use Modules\Site\Entities\Site;
use Modules\Site\Entities\CityRegion;
use Modules\Site\Entities\TariffRegion;

/**
 * Class SiteRepositoryEloquent
 * @package namespace App\Repositories;
 */
class SiteRepositoryEloquent extends BaseRepository implements SiteRepository
{
    /**
     * Specify Model class name
     *
     * @return string
     */
    public function model()
    {
        return Site::class;
    }
    /**
     * Boot up the repository, pushing criteria
     */
    public function boot()
    {
        $this->pushCriteria(app(RequestCriteria::class));
    }
    /* public function getRegionsAdnCityes()
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getRegionsAdnCityes()
    {
        $regions = TariffRegion::where('vTariffRegionCode', 'NOT LIKE', '%-%')->get();
        //
        $items = [];
        $arr = [];
        foreach($regions as $region)
        {
            $items[] = [
                'nTariffRegion_id'      => $region->nTariffRegion_id,
                'vTariffRegion'         => $region->vTariffRegion,
                'vTariffRegionCode'     => $region->vTariffRegionCode,
                'vTariffRegionShort'    => $region->vTariffRegionShort,
                'cityes'                => CityRegion::where('vTariffRegionCode', $region->vTariffRegionCode)->get()->toArray(),
            ];
            
            //$arr[] = [
            //    'name' => $region->vTariffRegion,
            //    'code' => $region->vTariffRegionCode,
            //];
        }
        
        //\Redis::set('regionsOld', collect($arr));
        
        return $items;
    }
    /* public function setAliases
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function setAliases()
    {
        if(\Request::segment(1) == 'admin')
        {
            return 'admin/*';
        }
        elseif(\Request::segment(1) == 'captcha')
        {
            return 'captcha/*';
        }
        elseif(\Request::segment(1) == 'socket.io')
        {
            return 'socket.io/*';
        }
        else
        {
            return '[\/\w\.-]*';
        }
    }
    /* public function mb_ucfirst
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function mb_ucfirst($str, $encoding = "UTF-8", $lower_str_end = false) {
        $first_letter = mb_strtoupper(mb_substr($str, 0, 1, $encoding), $encoding);
        $str_end = "";
        if ($lower_str_end) {
          $str_end = mb_strtolower(mb_substr($str, 1, mb_strlen($str, $encoding), $encoding), $encoding);
        }
        else {
          $str_end = mb_substr($str, 1, mb_strlen($str, $encoding), $encoding);
        }
        $str = $first_letter . $str_end;
        return $str;
    }
    /* public function getRC
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getRC()
    {
        $redis = \Redis::get('tariffs:all');
        $response = json_decode($redis);
        $collection = collect($response->message);
        //dd($collection);
        
        $filtered = $collection->where('parentid', '1Y');
        $regions = $filtered->all();
        $newReg = [];
        foreach($regions as $region)
        {
            $data = explode('|', $region->descr);
            
            $newReg[] = (object)[
                'name' => $data[0],
                'code' => $data[1],
                'short_name' => $data[2],
                'citys' => [],
            ];
        }
        $data = [];
        foreach($newReg as $k => $reg)
        {
            if($reg->code == 'msk')
            {
                $msk = $reg;
                unset($newReg[$k]);
            }
            
            if($reg->code == 'spb')
            {
                $spb = $reg;
                unset($newReg[$k]);
            }
        }
        array_unshift($newReg, $msk, $spb);
        
        return $newReg;
    }
    /* public function getTariffRegionMainTelefon
     * @param $id
     *-----------------------------------
     *| Тариф по регионам Частным клиентам - Sim для телефона :region - регион (msk,spb...)
     *-----------------------------------
     */
    public function getTariffRegionMainTelefon($region = 'msk')
    {
        $redis = \Redis::get('tariffs:all');
        $response = json_decode($redis);
        $collection = collect($response->message);
        //dd($collection);
        // Region
        $regionCode = $region;
        $region = $collection->filter(function ($value, $key) use ($regionCode) {
            $name = explode('|', $value->descr);
            if(in_array($regionCode, $name))
            {
                return $value;
            }
        });
        $region = $region->first();
        //Type
        $client = 'Частным клиентам';
        $type = $collection->where('parentid', $region->id);
        $type = $type->where('descr', $client)->first();
        //Types
        $types = $collection->where('parentid', $type->id);
        //Tariffs
        $tipTarifa = 'Sim для телефона';
        $tariffs = $types->where('descr', $tipTarifa)->first();
        //Get Tariff
        $arr = $collection->where('parentid', $tariffs->id)->sortBy('code');
        $data = [];
        if($arr)
        foreach($arr as $item)
        {
            $catalog = $collection->where('parentid', $item->id);
            $price = $catalog->where('descr', 1)->first();
            $prices = $collection->where('parentid', $price->id);
            $dataPrices = [];
            foreach($prices as $pr)
            {
                if($pr->code == 1)
                {
                    $dataPrices['activate'] = [
                        'label' => $pr->descr,
                        'txt' => $pr->answer,
                        'value' => str_replace(' Р', '', $pr->answer),
                    ];
                }
                if($pr->code == 2)
                {
                    $dataPrices['balance'] = [
                        'label' => $pr->descr,
                        'txt' => $pr->answer,
                        'value' => str_replace(' Р', '', $pr->answer),
                    ];
                }
                if($pr->code == 3)
                {
                    $dataPrices['total'] = [
                        'label' => $pr->descr,
                        'txt' => $pr->answer,
                        'value' => str_replace(' Р', '', $pr->answer),
                    ];
                }
                if($pr->code == 4)
                {
                    $dataPrices['internet'] = [
                        'label' => $pr->descr,
                        'txt' => $pr->answer,
                        'value' => str_replace(' Р', '', $pr->answer),
                    ];
                }
                if($pr->code == 5)
                {
                    $dataPrices['garantiya'] = [
                        'label' => $pr->descr,
                        'txt' => $pr->answer,
                    ];
                }
            }
            //
            $blocks = $catalog->where('descr', 2)->first();
            $blocks = $collection->where('parentid', $blocks->id)->sortBy('code');
            $dataBlocks = [];
            foreach($blocks as $block)
            {
                $descr = explode('|', $block->descr);
                $answer = explode('|', $block->answer);
                $dataBlocks[] = [
                    'name' => !empty($descr[0]) ? $descr[0] : '',
                    'color' => !empty($descr[1]) ?$descr[1] : '',
                    'txt' => !empty($answer[0]) ?$answer[0] : '',
                    'min' => !empty($answer[1]) ? $answer[1] : 0,
                    'max' => !empty($answer[2]) ? $answer[2] : 0,
                ];
            }
            $info = $catalog->where('descr', 3)->first();
            $info = $collection->where('parentid', $info->id)->sortBy('code');
            //$dataInfo = [];
            //foreach($info as $infoItem)
            //{
            //    $infoArr = $collection->where('parentid', $infoItem->id)->sortBy('code');
            //    foreach($infoArr as $infoA)
            //    {
            //        $i = explode('|', $infoA->descr);
            //        $ia[] = [
            //            'label' => !empty($i[0]) ? $i[0] : '',
            //            'region'=> !empty($i[1]) ? $i[1]: '',
            //            'city'  => !empty($i[2]) ? $i[2] : '',
            //            'txt'   => $infoA->answer,
            //        ];
            //    }
            //    
            //    $dataInfo[] = [
            //        'name' => $infoItem->descr,
            //        'txt' => $infoItem->answer,
            //        'data' => $ia,
            //    ];
            //}
            
            $data[] = [
                'id'    => $item->id,
                'name'  => $item->descr,
                'blocks' => $dataBlocks,
                'prices' => $dataPrices,
                'info' => $info,
            ];
        }
        $data = collect($data);
        return $data;
    }
    
    
}
