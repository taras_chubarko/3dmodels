<?php

namespace Modules\Taxonomy\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Taxonomy\Repositories\TaxonomyRepository;

class TaxonomyController extends Controller
{
	/*
	 *
	 */
	protected $taxonomy;
	/* public function __construct
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	public function __construct(TaxonomyRepository $taxonomy)
	{
		$this->taxonomy = $taxonomy;
	}
	/*
	 *
	 */
	public function index()
	{
		$taxonomy = $this->taxonomy->paginate(20);
		
		return view('admin::taxonomy.index', compact('taxonomy'));
	}
	
	/* public function create
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function create() {
		return view('admin::taxonomy.create');
	}
	
	/* public function store
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function store(\Modules\Taxonomy\Http\Requests\CreateReq $request) {
		
		$this->taxonomy->create($request->all());
		
		return redirect()->route('admin.taxonomy.index')->with('success', 'Новый словарь создан.');
	}
	
	/* public function edit
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function edit($id) {
		
		$taxonomy = $this->taxonomy->find($id);
		
		return view('admin::taxonomy.edit', compact('taxonomy'));
	}
	
	/* public function update
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function update(\Modules\Taxonomy\Http\Requests\CreateReq $request, $id) {
		
		$taxonomy = $this->taxonomy->update($request->all(), $id);
		
		return redirect()->route('admin.taxonomy.index')->with('success', 'Словарь таксономии обновлено.');
	}
	
	/* public function destroy
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function destroy($id) {
		
		$taxonomy = $this->taxonomy->find($id);
		$items = \TaxonomyTerm::findWhere(['taxonomy_id' => $id]);
		foreach($items as $item)
		{
		    $item->delete();
		}
		$taxonomy->delete();
	}
	
	/* public function show
	 * @param $id
	 *-----------------------------------
	 *|
	 *-----------------------------------
	 */
	
	public function show($id) {
		
		$taxonomy = $this->taxonomy->find($id);
		
		return view('admin::taxonomy.show', compact('taxonomy'));
	}
}
