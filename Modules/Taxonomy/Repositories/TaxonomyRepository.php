<?php

namespace Modules\Taxonomy\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface TaxonomyRepository
 * @package namespace App\Repositories;
 */
interface TaxonomyRepository extends RepositoryInterface
{
    //
}
