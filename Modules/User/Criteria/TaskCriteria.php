<?php

namespace Modules\User\Criteria;

use Prettus\Repository\Contracts\CriteriaInterface;
use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Class TaskCriteria
 * @package namespace App\Criteria;
 */
class TaskCriteria implements CriteriaInterface
{
    public $user;
    public $tab;
    public $category;
    /* public function __construct
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function __construct($user, $tab, $category)
    {
        $this->user     = $user;
        $this->tab      = $tab;
        $this->category = $category;
    }
    /**
     * Apply criteria in query repository
     *
     * @param                     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     */
    public function apply($model, RepositoryInterface $repository)
    {
        switch($this->tab)
        {
            case 'current':
                $array = $this->current();
                $model = $model->whereIn('id', $array)->orderBy('created_at', 'DESC');
		if(\Request::has('category_id') && \Request::get('category_id') != 'all')
		{
		    $model = $model->whereHas('category', function($query){
			$query->where('category_id', \Request::get('category_id'));
		    });
		}
	    break;
            
            case 'completed':
                $array = $this->completed();
                $model = $model->whereIn('id', $array);
		if(\Request::has('category_id') && \Request::get('category_id') != 'all')
		{
		    $model = $model->whereHas('category', function($query){
			$query->where('category_id', \Request::get('category_id'));
		    });
		}
            break;
            
            case 'deleted':
                $array = $this->current();
                $model = $model->whereIn('id', $array)->whereNotNull('deleted_at')->orderBy('deleted_at', 'DESC')->withTrashed();
		if(\Request::has('category_id') && \Request::get('category_id') != 'all')
		{
		    $model = $model->whereHas('category', function($query){
			$query->where('category_id', \Request::get('category_id'));
		    });
		}
	    break;
            
        }
        
        return $model;
    }
    /* public function current
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function current()
    {
        $array = array();

        if(session('inRole') == 'customer')
        {
            $jobs = \DB::table('jobs')
            ->where('user_id', $this->user->id)
            ->whereIn('status', [0, 1, 2])
            ->select('id')->get();
            $array = $jobs->pluck('id')->toArray();
        }
        
        if(session('inRole') == 'executor')
	{
	    $currentjobs = \DB::table('users_employment')
            ->where('user_id', $this->user->id)
            ->where('status', 0)
            ->select('jobs_id')->get();
            $array = $currentjobs->pluck('jobs_id')->toArray();
	}
        
        
        return $array;
    }
    /* public function completed
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function completed()
    {
        $array = array();
        
        if(session('inRole') == 'customer')
        {
            $jobs = \DB::table('jobs')
            ->where('user_id', $this->user->id)
            ->whereIn('status', [3, 4])
            ->select('id')->get();
            $array = $jobs->pluck('id')->toArray();
        }
        
        if(session('inRole') == 'executor')
	{
	    $currentjobs = \DB::table('users_employment')
            ->where('user_id', $this->user->id)
            ->where('status', 1)
            ->select('jobs_id')->get();
            $array = $currentjobs->pluck('jobs_id')->toArray();
	}
        
        return $array;
    }
    
}
