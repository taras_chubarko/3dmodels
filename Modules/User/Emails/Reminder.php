<?php

namespace Modules\User\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Reminder extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user, $credentials, $reminder, $request)
    {
        $this->user         = $user;
        $this->credentials  = $credentials;
        $this->reminder     = $reminder;
        $this->request      = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $user         = $this->user;
        $credentials  = $this->credentials;
        $reminder     = $this->reminder;
        $request      = $this->request;
        
        $subject['uk'] = 'Відновлення паролю';
        $subject['ru'] = 'Восстановление пароля';
        $subject['en'] = 'Password recovery';
        
        return $this->view('site::mail.mail-reminder-'.$request->lang, compact('user', 'credentials', 'reminder'))
        ->to($user->email)
        ->subject($subject[$request->lang].' "'.config('app.name').'"');
    }
}
