<?php

namespace Modules\User\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cartalyst\Sentinel\Users\UserInterface;
use Cartalyst\Sentinel\Users\EloquentUser as CartalystUser;
use Cviebrock\EloquentSluggable\Sluggable;

class User extends CartalystUser implements Transformable 
{
    use Sluggable;
    use SoftDeletes;
    use TransformableTrait;

    
    protected $fillable = ['email', 'password', 'first_name', 'last_name',  'permissions'];
    
    protected $table = 'users';
    
    protected $dates = ['deleted_at'];
    
    //protected $attributes = array(
    //    'CountReviews' => '',
    //);
    //
    //protected $appends = ['CountReviews'];
    
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'first_name'
            ]
        ];
    }
    /* public function getFullNameAttribute
     * @param $id
     *-----------------------------------
     *| полное имя
     *-----------------------------------
     */
    public function getFullNameAttribute()
    {
        //$name[] = $this->first_name;
        //$name[] = $this->last_name;
        //return implode(' ', $name);
        return $this->first_name;
    }
    /*
     * Аватарка
     */
    public function avatar()
    {
        return $this->belongsToMany('Modules\Filem\Entities\Filem', 'users_avatar', 'user_id', 'fid')
        ->withPivot('fid');
    }
    /*
     * забаненый пользователь
     */
    public function ban()
    {
        return $this->belongsToMany('Modules\User\Entities\User', 'users_ban')
        ->withTimestamps();
    }
    /*
     * Роли
     */
    public function role()
    {
        return $this->belongsToMany('Modules\User\Entities\User', 'role_users')
        ->withPivot('role_id')
        ->leftJoin('roles', 'roles.id', '=', 'role_users.role_id')
        ->select(
            "roles.id as pivot_id",
            "roles.name as pivot_name",
            "roles.slug as pivot_slug",
            "roles.permissions as pivot_permissions"
        );
    }
    /*
     *
     */
    public function isOnline()
    {
        return \Cache::has('user-is-online-' . $this->id);
    }
    /* public function social
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function social()
    {
        return $this->hasMany('Modules\User\Entities\UserSocial');
    }
    /* public function getUserRolesAttribute
     * @param $id
     *-----------------------------------
     *| Массив ролей пользователя
     *-----------------------------------
     */
    public function getUserRolesAttribute()
    {
        $items = array();
        foreach($this->role as $role)
        {
            $items[] = $role->pivot->name;
        }
        return implode(',', $items);
    }
    /* public function getDateAttribute
     * @param $id use Illuminate\Http\Request;
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getFlagAttribute()
    {
        if($this->status == 0)
        {
            return '<span class="text-warning"><i class="fa fa-hourglass-start"></i> Не активный</span>';/*'ptEOzaYeoze1LNpG4RZT' => 'Not actively',*/
        }
        if($this->status == 2)
        {
            return '<span class="text-danger"><i class="fa fa-lock"></i> Заблокирован</span>'; //'c0MdvE2REYDIIADCGCLb' => 'Blocked',
        }
        return '<span class="text-success"><i class="fa fa-check"></i> Активный</span>'; //'0kSkBqsOinKQ4FfoLRBH' => 'Active',
    }
    /* public function name
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getRolesArrAttribute()
    {
        $items = array();
        foreach($this->role as $role)
        {
            $items[$role->pivot->id] = $role->pivot->id;
        }
        return $items;
    }
    /* public function getStatusAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getStatusAttribute()
    {
        $user = $this;
        $activation = \Activation::exists($user);
        $status = 0;
        if($activation == false)
        {
            $status = 0;
        }
        if($activation && $activation->completed == 0)
        {
            $status = 0;
        }
        
        if ($activation = \Activation::completed($user))
        {
            $status = 1;
        }
        else
        {
            $status = 0;
        }
        
        if($user->ban->count())
        {
            $status = 2;
        }
        
        return $status;
    }
    /*
     *
     */
    public function scopeRol($query, $id)
    {
      return $query->role()->where('role_id', $id);
    }
    /* public function getFieldCreatedAttribute
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function getFieldCreatedAttribute()
    {
        $field = new \stdClass;
        $field->created = $this->created_at;
        $field->diff = diffDate($field->created);
        return $field;
    }
    /* public function name
     * @param $id
     *-----------------------------------
     *| Аватар
     *-----------------------------------
     */
    public function getAvaAttribute()
    {
        $data = new \stdClass;
        $avatar = data_get($this, 'avatar.0');
        if($avatar)
        {
            $data->image = $avatar->filename;
        }
        else
        {   
            $name = str_random(10).'.jpg';
            $ava = \Avatar::create($this->fullName)->save(public_path('/uploads/avatar/'.$name), 100);
            $fid = \Filem::create([
                'user_id'   => $this->id,
                'original'  => $name,
                'filename'  => $name,
                'uri'       => 'avatar',
                'ext'       => 'jpg',
                'filemime'  => 'image/jpeg',
                'filesize'  => 1,
                'status'    => 1,
            ]);
            
            $this->avatar()->attach($this->id,['fid' => $fid->id]);
            
            $data->image = 'nouser.png';
        }
        return $data;
    }
    

}
