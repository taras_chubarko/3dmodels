<?php

namespace Modules\User\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\User\Repositories\UserRepository;
use Modules\User\Events\UserRegisterEvent;
use Modules\User\Events\ReminderEvent;
use Modules\Host\Entities\HostBook;
use Modules\User\Entities\UserSocial;
use Modules\User\Entities\UserProvide;
use Modules\User\Emails\ProvideToAdmin;
use Faker\Factory as Faker;
use Illuminate\Pagination\LengthAwarePaginator;


class UserController extends Controller
{
    /*
     *
     */
    protected $user;
    /*
     *
     */
    public function __construct(UserRepository $user)
    {
        $this->user = $user;
    }
    /* public function login_submit
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function login_submit(Request $request)
    {
        try
        {
            $credentials = [
                'email'    => $request->email,
                'password' => $request->password,
            ];
            $user = \Sentinel::findByCredentials($credentials);
	    //dd($user);
            if($user){
		//if($user->ban->count())
		//{
		//    return response()->json(['error' => [0 => 'User is blocked']], 442);
		//}
		//else
		//{
		    $user = \Sentinel::authenticate($credentials);
		    
		    if($user)
		    {
			//\Sentinel::login($user);
			$user->load('roles');
			//return redirect()->back()->with('error', 'Вы успешно вошли на сайт.');
			if($user->inRole('admin'))
			{
			    $data['redirect'] = '/';
			    $data['cartalyst_sentinel'] = $request->session()->get('_token');
			    $data['expiration'] = \Carbon::now()->addMinutes(config('session.lifetime'))->timestamp;
			    $data['user'] = $user;
			    return response()->json($data, 200);
			}
			else
			{
			    $data['redirect'] = '/';
			    $data['cartalyst_sentinel'] = $request->session()->get('_token');
			    $data['expiration'] = \Carbon::now()->addMinutes(config('session.lifetime'))->timestamp;
			    $data['user'] = $user;
			    \Session::flash('success', 'You have successfully entered the site.');
			    return response()->json($data, 200);
			}
		    }
		    else
		    {
			//return redirect()->back()->with('error', 'Не верный логин или пароль');
			$msg['uk'] = [0 => 'Невірний логін або пароль.'];
			$msg['ru'] = [0 => 'Не верный логин или пароль.'];
			$msg['en'] = [0 => 'Wrong login or password.'];
			return response()->json(['error' => $msg], 442);
		    }
		//}
	    }
	    else{
		//\Session::flash('info', 'Пользователь не существует.');
		$msg['uk'] = [0 => 'Аккаунт не існує.'];
		$msg['ru'] = [0 => 'Аккаунт не существует.'];
		$msg['en'] = [0 => 'Account does not exist.'];
		return response()->json(['error' => $msg], 442);
	    }
        }
        catch(\Cartalyst\Sentinel\Checkpoints\NotActivatedException $e){
            //return redirect()->back()->with('error', 'Пользователь не активирован.');
	    $msg['uk'] = [0 => 'Аккаунт не активований.'];
	    $msg['ru'] = [0 => 'Аккаунт не активирован.'];
	    $msg['en'] = [0 => 'Account is not activated.'];
            return response()->json(['error' => $msg], 442);
        }
        catch(\Cartalyst\Sentinel\Checkpoints\ThrottlingException $e){
            //return redirect()->back()->with('error', 'Пользователь заблокирован.');
	    $msg['uk'] = [0 => 'Аккаунт заблокований.'];
	    $msg['ru'] = [0 => 'Аккаунт заблокирован.'];
	    $msg['en'] = [0 => 'Account is blocked.'];
            return response()->json(['error' => $msg], 442);
        }
    }
    /* public function logout
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function logout(Request $request)
    {
        $user = \Sentinel::getUser();
        //\Cache::forget('user-is-online-' . $user->id);
        \Sentinel::logout($user, true);
	
	if($request->ajax())
	{
	    return response()->json('OK', 200);
	}
	else
	{
	    return redirect('/');
	}
    }
    /* public function api_logout
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function api_logout(Request $request)
    {
	$user = \Sentinel::getUser();
        //\Cache::forget('user-is-online-' . $user->id);
        \Sentinel::logout($user, true);
    }
    /* public function reminder_submit
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function reminder_submit(Request $request)
    {
	$rules = ['captcha' => 'captcha'];
	$validator = \Validator::make($request->all(), $rules);
	if ($validator->fails())
	{
	    $msg['uk'] = [0 => 'Невірний код Каптчі.'];
	    $msg['ru'] = [0 => 'Неверный код Каптчи.'];
	    $msg['en'] = [0 => 'Invalid Captcha code.'];
	    return response()->json(['error' => $msg], 442);
	}
	else
	{
	    $credentials = [
		'email'    => $request->email,
	    ];
	    $user = \Sentinel::findByCredentials($credentials);
	    //
	    if($user)
	    {
		$reminder = \Reminder::create($user);
		event(new ReminderEvent($user, $credentials, $reminder));
	    }
	    else
	    {
		$msg['uk'] = [0 => 'Користувач з таким email не існує!'];
		$msg['ru'] = [0 => 'Пользователь с таким email не существует!'];
		$msg['en'] = [0 => 'User with such mail does not exist!'];
		return response()->json(['error' => $msg], 442);
	    }
	}
    }
    /* public function reminder_code
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function reminder_code($code)
    {
	$reminder = \DB::table('reminders')->where('code', $code)->first();
	
	return view('site::user.page-reminder', compact('reminder'));
    }
    /* public function reminder_set_new_password
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function reminder_set_new_password(Request $request)
    {
	$rules = ['captcha' => 'captcha'];
	$validator = \Validator::make($request->all(), $rules);
	if ($validator->fails())
	{
	    $msg['uk'] = [0 => 'Невірний код Каптчі.'];
	    $msg['ru'] = [0 => 'Неверный код Каптчи.'];
	    $msg['en'] = [0 => 'Invalid Captcha code.'];
	    return response()->json(['error' => $msg], 442);
	}
	else
	{
	    $reminder = \DB::table('reminders')->where('code', $request->code)->first();
	    if($reminder)
	    {
		$user = \Sentinel::findById($reminder->user_id);
		$user->load('roles');
		if ($remind = \Reminder::complete($user, $request->code, $request->password))
		{
		    \Sentinel::login($user);
		    
		    $data['redirect'] = '/';
		    $data['cartalyst_sentinel'] = $request->session()->get('_token');
		    $data['expiration'] = \Carbon::now()->addMinutes(config('session.lifetime'))->timestamp;
		    $data['user'] = $user;
		    return response()->json($data, 200);
		}
		else
		{
		    $msg['uk'] = [0 => 'Код не дійсний!'];
		    $msg['ru'] = [0 => 'Код недействителен!'];
		    $msg['en'] = [0 => 'The code is not valid!'];
		    return response()->json(['error' => $msg], 442);
		}
	    }
	    else
	    {
		$msg['uk'] = [0 => 'Код не дійсний!'];
		$msg['ru'] = [0 => 'Код недействителен!'];
		$msg['en'] = [0 => 'The code is not valid!'];
		return response()->json(['error' => $msg], 442);
	    }
	    
	}
    }
    /* public function register
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function register(Request $request)
    {
	$credentials = [
            'email'    => $request->email,
        ];
        $user = \Sentinel::findByCredentials($credentials);
	
	if($user)
	{
	    $msg['uk'] = [0 => 'Користувач з цією поштою вже зареєстрований!'];
	    $msg['ru'] = [0 => 'Пользователь с этой почтой уже зарегистрирован!'];
	    $msg['en'] = [0 => 'The user with this mail is already registered!'];
	    return response()->json(['error' => $msg], 442);
	}
	else
	{
	    $rules = ['captcha' => 'captcha'];
	    $validator = \Validator::make($request->all(), $rules);
	    if ($validator->fails())
	    {
		$msg['uk'] = [0 => 'Невірний код Каптчі.'];
		$msg['ru'] = [0 => 'Неверный код Каптчи.'];
		$msg['en'] = [0 => 'Invalid Captcha code.'];
		return response()->json(['error' => $msg], 442);
	    }
	    else
	    {
		$first_name = explode('@', $request->email);
		$credentials = [
		    'email'    	=> $request->email,
		    'first_name'    => $first_name[0],
		    'password' 	=> $request->password,
		];
		    
		$user = \Sentinel::registerAndActivate($credentials);
		$user->load('roles');
		$rore_user = \Sentinel::findRoleById(2);
		$rore_user->users()->attach($user);
		
		\Sentinel::login($user);
		
		event(new UserRegisterEvent($user));
		
		$data['redirect'] = '/';
		$data['cartalyst_sentinel'] = $request->session()->get('_token');
		$data['expiration'] = \Carbon::now()->addMinutes(config('session.lifetime'))->timestamp;
		$data['user'] = $user;
		return response()->json($data, 200);
	    }
	}
    }
    /* public function user_switch
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function user_switch($id)
    {
        if (\Sentinel::check())
	{
	    \Sentinel::logout();
	}
	    
	$user = \Sentinel::findById($id);
	    
	\Sentinel::login($user);
	    
	return redirect('/');
    }
    /* public function captcha
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function captcha()
    {
	$captcha = captcha_src('mini');
	return response()->json($captcha, 200);
    }
    /* public function social_login
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function social_login(Request $request)
    {
	$credentials = [
            'email' => $request->email,
        ];
            
        $user = \Sentinel::findByCredentials($credentials);
	
	if($user)
	{
	    $this->user->socialLogin($request, $user);
	    \Sentinel::login($user);
	    $data['redirect'] = '/';
	    $data['cartalyst_sentinel'] = $request->session()->get('_token');
	    $data['expiration'] = \Carbon::now()->addMinutes(config('session.lifetime'))->timestamp;
	    $data['user'] = $user;
	    return response()->json($data, 200);
	}
	else
	{
	    $checkRemove = \DB::table('users')
	    ->where('email', $request->email)
	    ->whereNotNull('deleted_at')->first();
	    if($checkRemove)
	    {
		$msg['uk'] = [0 => "Користувача було видалено. Будь ласка, зв'яжіться з адміністратором."];
		$msg['ru'] = [0 => 'Пользователь удален. Пожалуйста, свяжитесь с администратором.'];
		$msg['en'] = [0 => 'User was removed. Please contact with adminisntator.'];
		
		return response()->json($msg, 442);
	    }
	    else
	    {
		$user = $this->user->socialRegister($request);
		\Sentinel::login($user);
		$data['redirect'] = '/';
		$data['cartalyst_sentinel'] = $request->session()->get('_token');
		$data['expiration'] = \Carbon::now()->addMinutes(config('session.lifetime'))->timestamp;
		$data['user'] = $user;
		return response()->json($data, 200);
	    }
	}
    }
    /* public function profile
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function profile(Request $request)
    {
	$faker = Faker::create('uk_UA');
	
	$notify = [];
	$blocks = [];
	
	\Carbon::setLocale('uk');
	foreach(range(1, 3) as $i)
        {
	    $name = $faker->lastName.' '.$faker->firstName;
	    $notifyType = $faker->randomElement(['work', 'gallery', 'info']);
	    $notify[] = [
		'user' => [
		    'link' 	=> '/profile',
		    'avatar' 	=> 'http://erundit.ru/image/cache/catalog/avatar/'.$faker->numberBetween(240, 310).'-200x200.jpg',
		    'name' 	=> $name,
		],
		'text' => $faker->text(200),
		'type' => [
		    'name' 	=> $notifyType,
		    'src' 	=> 'http://erundit.ru/image/cache/catalog/avatar/'.$faker->numberBetween(240, 310).'-200x200.jpg',
		    'link' 	=> '/profile',
		],
		'created' => \Carbon::parse($faker->date($format = 'Y-m-d', $max = 'now'))->diffForHumans(),
	    ];
	}
	
	//
	foreach(range(1, 14) as $i)
        {
	    $name = $faker->lastName.' '.$faker->firstName;
	    $tpl['product'] = [
		'tpl' => 'product',
		'img' => route('fake.image', ['217x160', $faker->numberBetween(100, 500)]),
		'division' => $faker->randomElement(['midi', 'maxi', 'mini', 'ultra', 'free']),
		'name' => $faker->sentence(3),
		'category' => $faker->word,
		'downloads' => $faker->numberBetween(0, 500),
		'comments' => $faker->numberBetween(0, 500),
		'views' => $faker->numberBetween(0, 500),
		'likes' => $faker->numberBetween(0, 500),
		'price' => $faker->numberBetween(0, 500),
		'user' => [
		    'link' 	=> '/profile',
		    'avatar' 	=> 'http://erundit.ru/image/cache/catalog/avatar/'.$faker->numberBetween(240, 310).'-200x200.jpg',
		    'name' 	=> $name,
		],
	    ];
	    
	    $tpl['gallery'] = [
		'tpl' => 'gallery',
		'img' => route('fake.image', ['217x160', $faker->numberBetween(100, 500)]),
		'name' => $faker->sentence(3),
		'category' => $faker->word,
		'scale' => '1:'.$faker->numberBetween(1, 100),
		'downloads' => $faker->numberBetween(0, 500),
		'comments' => $faker->numberBetween(0, 500),
		'views' => $faker->numberBetween(0, 500),
		'likes' => $faker->numberBetween(0, 500),
		'user' => [
		    'link' 	=> '/profile',
		    'avatar' 	=> 'http://erundit.ru/image/cache/catalog/avatar/'.$faker->numberBetween(240, 310).'-200x200.jpg',
		    'name' 	=> $name,
		],
	    ];
	    
	    $tpl['tema'] = [
		'tpl' => 'tema',
		'img' => route('fake.image', ['260x160', $faker->numberBetween(100, 500)]),
		'name' => $faker->sentence(3),
		'text' => $faker->text($maxNbChars = 200),
		'category' => $faker->words($nb = 3, $asText = false),
		'comments' => $faker->numberBetween(0, 500),
		'created' => \Carbon::parse($faker->date($format = 'Y-m-d', $max = 'now'))->format('d.m.Y'),
		'user' => [
		    'link' 	=> '/profile',
		    'avatar' 	=> 'http://erundit.ru/image/cache/catalog/avatar/'.$faker->numberBetween(240, 310).'-200x200.jpg',
		    'name' 	=> $name,
		],
	    ];
	    
	    $tpl['work'] = [
		'tpl' => 'work',
		'img' => route('fake.image', ['260x160', $faker->numberBetween(100, 500)]),
		'name' => $faker->sentence(3),
		'text' => $faker->text($maxNbChars = 100),
		'category' => $faker->words($nb = 3, $asText = false),
		'price' => $faker->numberBetween(0, 500),
		'created' => \Carbon::parse($faker->date($format = 'Y-m-d', $max = 'now'))->format('d.m.Y'),
		'user' => [
		    'link' 	=> '/profile',
		    'avatar' 	=> 'http://erundit.ru/image/cache/catalog/avatar/'.$faker->numberBetween(240, 310).'-200x200.jpg',
		    'name' 	=> $name,
		],
		'deadline' => $faker->numberBetween(0, 500),
		'bids' => $faker->numberBetween(0, 500),
		'tema' => $faker->randomElement(['3d модели', 'web', 'audio', 'video', 'Полиграфия']),
		'ico' => $faker->randomElement(['topcat-uniF109', 'topcat-uniF110', 'topcat-uniF10F', 'topcat-uniF101', 'topcat-uniF10E']),
	    ];
	    
	    
	    $blocks[] = $tpl[$faker->randomElement(['product', 'gallery', 'tema', 'work'])];
	}
	
	
	$response['notify'] = $notify;
	$response['blocks'] = $blocks;
	
	return response()->json($response, 200);
    }
    /* public function all_notifications
     * @param $id
     *-----------------------------------
     *|
     *-----------------------------------
     */
    public function all_notifications(Request $request)
    {
	$faker = Faker::create('uk_UA');
	
	$notify = [];
	
	\Carbon::setLocale('uk');
	foreach(range(1, 50) as $i)
        {
	    $name = $faker->lastName.' '.$faker->firstName;
	    $notifyType = $faker->randomElement(['work', 'gallery', 'info']);
	    $notify[] = [
		'user' => [
		    'link' 	=> '/profile',
		    'avatar' 	=> 'http://erundit.ru/image/cache/catalog/avatar/'.$faker->numberBetween(240, 310).'-200x200.jpg',
		    'name' 	=> $name,
		],
		'text' => $faker->text(200),
		'type' => [
		    'name' 	=> $notifyType,
		    'src' 	=> 'http://erundit.ru/image/cache/catalog/avatar/'.$faker->numberBetween(240, 310).'-200x200.jpg',
		    'link' 	=> '/profile',
		],
		'created' => \Carbon::parse($faker->date($format = 'Y-m-d', $max = 'now'))->diffForHumans(),
	    ];
	}
	
	$collection = collect($notify);
        //
        $currentPage = LengthAwarePaginator::resolveCurrentPage();
        $perPage = 10;
        $currentPageSearchResults = $collection->slice(($currentPage - 1) * $perPage, $perPage)->all();
        $data = new LengthAwarePaginator($currentPageSearchResults, $collection->count(), $perPage);
        $data->withPath('/api/v1/profile/activity/all-notifications');
        return response()->json($data, 200);
    }
    
}
