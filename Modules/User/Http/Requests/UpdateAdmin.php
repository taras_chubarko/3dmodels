<?php

namespace Modules\User\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAdmin extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rule = [];
        //$rule['last_name']              = 'required';
        $rule['first_name']             = 'required';
        $rule['email']                  = 'required|email';
        
        if(\Request::has('password'))
        {
            $rule['password']               = 'required|min:6|confirmed';
            $rule['password_confirmation']  = 'required|min:6';
        }
        
        return $rule;
    }
    
    protected function getValidatorInstance()
    {
        $validator = parent::getValidatorInstance();
     
        $validator->setAttributeNames([
            'last_name'     => '"Фамилия"',
            'first_name'    => '"Имя"',
            'email' 	    => '"Почта"',
	    'password'      => '"Пароль"',
	    'password_confirmation' => '"Повтор пароля"',
        ]);
     
        return $validator;
    }
}
