<?php

Route::group(['middleware' => 'web', 'prefix' => 'api/v1', 'namespace' => 'Modules\User\Http\Controllers'], function()
{
    Route::post('login', [
	'as' 		=> 'api.v1.login',
	'uses' 		=> 'UserController@login_submit'
    ]);
    
    Route::post('logout', [
	'as' 		=> 'api.v1.logout',
	'uses' 		=> 'UserController@logout'
    ]);
    
    Route::post('register', [
	'as' 		=> 'api.v1.register',
	'uses' 		=> 'UserController@register'
    ]);
    
    Route::post('captcha', [
	'as' 		=> 'api.v1.captcha',
	'uses' 		=> 'UserController@captcha'
    ]);
    
    Route::post('user/reminder', [
	'as' 		=> 'api.v1.reminder',
	'uses' 		=> 'UserController@reminder_submit'
    ]);
    
    Route::put('user/reminder', [
	'as' 		=> 'api.v1.reminder',
	'uses' 		=> 'UserController@reminder_set_new_password'
    ]);
    
    Route::post('social/login', [
	'as' 		=> 'api.v1.social.login',
	'uses' 		=> 'UserController@social_login'
    ]);
    
//    Route::post('profile', [
//	'as' 		=> 'api.v1.profile',
//	'uses' 		=> 'UserController@profile'
//    ]);
    
});

Route::group(['middleware' => 'auth.apikey', 'prefix' => 'api/v1', 'namespace' => 'Modules\User\Http\Controllers'], function()
{
    Route::post('profile', [
	'as' 		=> 'api.v1.profile',
	'uses' 		=> 'UserController@profile'
    ]);
    
    Route::post('profile/activity/all-notifications', [
	'as' 		=> 'api.v1.profile.activity.all.notifications',
	'uses' 		=> 'UserController@all_notifications'
    ]);
});

/*
 * Админка
 */
Route::group(['middleware' => ['web', 'Modules\Admin\Http\Middleware\AdminMDW'], 'prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Modules\User\Http\Controllers'], function()
{
    Route::resource('users', 'AdminUserController');
    
    Route::post('users/{id}/activate', [
        'as' => 'users.activate',
        'uses' => 'AdminUserController@activate'
    ]);
    
    Route::post('users/{id}/ban', [
        'as' => 'users.ban',
        'uses' => 'AdminUserController@ban'
    ]);
    
    Route::post('users/{id}/unban', [
        'as' => 'users.unban',
        'uses' => 'AdminUserController@unban'
    ]);
   
});
