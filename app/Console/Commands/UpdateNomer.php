<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Ixudra\Curl\Facades\Curl;

class UpdateNomer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vip:nomer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Обновление номеров';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $regions = config('site.regions');
        foreach($regions as $region)
        {
            //$response = file_get_contents(config('app.api_host').'/tools/numbers/'.$region['name'].'?Authorization='.config('app.api_key'));
            $response =  Curl::to(config('app.api_host').'/tools/numbers/'.$region['name'])
            ->withHeader('Authorization:'.config('app.api_key'))
            ->get();
            $response = json_decode($response);
            if($response->error == false)
            {
                \Redis::set('nomers:'.$region['code'], collect($response->message));
            }
        }
        
    }
}
