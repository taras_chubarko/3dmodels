<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class UpdateTariff extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'vip:tariff';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Save tariff to Redis from api.vipabonent.ru';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $response = file_get_contents(config('app.api_host').'/tools/tariffs?Authorization='.config('app.api_key'));
        \Redis::set('tariffs:all', $response);
        
        $regions = collect(\Site::getRC())->toJson();
        \Redis::set('regions', $regions);
    }
}
