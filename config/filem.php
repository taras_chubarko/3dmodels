<?php

return [
    'name' => 'Filem',
    'images_table' => [
        'users_avatar',
        'taxonomy_term_image',
        'products_image',
    ],
];
