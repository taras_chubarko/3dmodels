$(document).ready(function() {
    /*
     *
     */
     $.ajaxSetup({
        headers: {
            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
        },
	beforeSend:function(xhr){
            $('body').append('<div id="ajax-load"></div>');
        },
        complete:function(data){
            $('#ajax-load').remove();
        }
    });
    
    
     $( document ).ajaxError(function( event, jqxhr, settings, thrownError ) {
          $('#ajax-load').remove();
          if (jqxhr.status === 500) {
              alert(jqxhr.statusText);
          }
          else{
              Alerts.message('error', 'Error!', jqxhr.responseJSON);
          }
     });
     /*
      *
      */
     //Locations.init();
     /*
     *
     */
     function ajaxSubmit() {
          $('.ajax-submit').on('submit', function(){
               var self = $(this);
               var data = self.serializeArray();
               var type = 'POST';
               if (data['_method']) {
                    type = data['_method'];
               }
               $.ajax({
                    url: self.attr('action'),
                    type: 'POST',
                    data: data,
                    success: function(data) {
                         if (data.redirect) {
                              window.location.href = data.redirect;
                         }
                         if (data.reload) {
                              window.location.reload();
                         }
                         if (data.success) {
                              Alerts.message('success', 'Success!', data);
                         }
                         if (data.confirm) {
                              confirmAjax(data);
                         }
                    }
               });
             return false;
          });
     } ajaxSubmit();
    /*
     *
     */
     $('.ajax-link').bind('click', function(){
          var self = $(this);
          $.post(self.attr('href'), function(data){
               if (data.redirect) {
                  window.location.href = data.redirect;
               }
               if (data.success) {
                  Alerts.message('success', 'Success!', data);
               }
               if (data.remove) {
                    self.parent().parent().remove();
                    $.Notification.autoHideNotify('success','top center', 'Success!', data.message);
               }
          });
          return false;
     });
    /*
     *
     */
    jQuery.each( [ "put", "delete" ], function( i, method ) {
        jQuery[ method ] = function( url, data, callback, type ) {
          if ( jQuery.isFunction( data ) ) {
            type = type || callback;
            callback = data;
            data = undefined;
          }
       
          return jQuery.ajax({
            url: url,
            type: method,
            dataType: type,
            data: data,
            success: callback
          });
        };
    });
    /*
     *
     */
    $('.users-tabs a').on('click', function(){
        var self = $(this);
        var link = self.attr('href');
        window.location.href = link;
    });
    /*
     *
     */

    /*
     *
     */
    //$('#checkboxAll').change(function () {
    //    $('#datatable-editable tbody input:checkbox').prop('checked', $(this).prop("checked"));
    //});
    ///*
    // *
    // */
    //$('.form-editable').on('submit', function(){
    //      var countChk = $('#datatable-editable tbody input:checkbox:checked').length;
    //      if (countChk == 0) {
    //           swal({
    //               title: "Внимание!",
    //               text: "Необходимо выбрать отзывы",
    //               type: "warning"
    //           });
    //           return false;
    //      }
    //});
    
     if($('.tin').length) {
          $('.tin').mask('99:99');
     }
     /*
      *
      */
     if($('.geocomplete').length) {
          $(".geocomplete").geocomplete({ details: '.address' });
     }
     /*
      *
      */
     if($('.summernote').length) {
          $('.summernote').summernote({
               height: 200,                 
               minHeight: null,            
               maxHeight: null,             
               focus: true           
          });
     }
     /*
      *
      */
     $('#shop_id').on('change', function(){
          var self = $(this);
          $.post('/product/get/category', {shop_id:self.val()}, function(data){
               $('#category-wrp').html(data.result);
               $('.selectpicker').selectpicker('refresh');
               category_change();
          });
     });
     /*
      *
      */
     function category_change() {
          $('#select-category').on('change', function(){
               var self = $(this);
               if (self.val() == 'category_add') {
                    $('.category_add').css('display', 'block');
                    $('#category-wrp').css('display', 'none');
               }
               else
               {
                    $('.category_add').css('display', 'none');
                    $('#category-wrp').css('display', 'block');
               }
          });
     }
     /*
      *
      */
     $('.form-product').bind("keypress", function(e) {
          if (e.keyCode == 13) {               
            e.preventDefault();
            return false;
          }
     });
     /*
      * Parser
      */
     
     //function parse() {
     //     var key = 0;
     //     $('.parse').on('click', function(){
     //          var self = $(this);
     //          alert('Дождитесь завершения работы парсера. Оно наступит после перезагрузки страницы.');
     //          process(self.attr('href'), key);
     //          return false;
     //     });
     //} parse();
     //
     //function process(url, key) {
     //    
     //    $.get(url, {page:key}, function(data){
     //        key += 1;
     //        if (key != data.count) {
     //          process(url, key);
     //        }
     //        else
     //        {
     //          window.location.reload();
     //        }
     //    });
     //}
     //
     //function clearParse() {
     //     var key = 1;
     //     $('#clearProduct').on('click', function(){
     //          var self = $(this);
     //          alert('Дождитесь завершения работы парсера. Оно наступит после перезагрузки страницы.');
     //          process2(self.attr('href'));
     //          return false;
     //     });
     //} clearParse();
     //
     //function process2(url) {
     //     var key = 1;
     //     $.get(url, {page:1}, function(data){
     //        //key += 1;
     //        if (key != data.count) {
     //          process2(url);
     //        }
     //        else
     //        {
     //          window.location.reload();
     //        }
     //     });
     //}
     /*
      *
      */
     $('.form-parser-product').on('submit', function(){
          var self = $(this);
          var dani = self.serializeArray();
          $.post(self.attr('action'), dani, function(data){
              
              window.location.reload();
          });
          return false;  
     });
     /*
      *
      */
     $('.user-activate').on('click', function(){
          var self = $(this);
          swal({   
               title: "You are sure?",   
               text: 'what you want to activate user',   
               type: "warning",   
               showCancelButton: true,   
               confirmButtonColor: "#DD6B55",   
               confirmButtonText: "Yes, let's activate",
               cancelButtonText: "Cancel",
               closeOnConfirm: false 
          }, function(){
               $.ajax({url: self.attr('href'), type: 'POST', success: function(data) {
                   //self.parent().parent().remove();
                   swal("OK!", 'The user activated', "success");
                   window.location.reload();
               }});
          });
          return false;  
     });
     /*
      *
      */
     $('.user-ban').on('click', function(){
          var self = $(this);
          swal({   
               title: "You are sure?",   
               text: 'what you want to block the user',   
               type: "warning",   
               showCancelButton: true,   
               confirmButtonColor: "#DD6B55",   
               confirmButtonText: "Yes, to block!",
               cancelButtonText: "Cancel",
               closeOnConfirm: false 
          }, function(){
               $.ajax({url: self.attr('href'), type: 'POST', success: function(data) {
                   //self.parent().parent().remove();
                   swal("OK!", 'The user is blocked', "success");
                   window.location.reload();
               }});
          });
          return false;  
     });
     /*
      *
      */
     $('.user-unban').on('click', function(){
          var self = $(this);
          swal({   
               title: "You are sure?",   
               text: 'what you want to unlock the user',   
               type: "warning",   
               showCancelButton: true,   
               confirmButtonColor: "#DD6B55",   
               confirmButtonText: "Yes, unblock!",
               cancelButtonText: "Cancel",
               closeOnConfirm: false 
          }, function(){
               $.ajax({url: self.attr('href'), type: 'POST', success: function(data) {
                   //self.parent().parent().remove();
                   swal("OK!", 'The user is unlocked', "success");
                   window.location.reload();
               }});
          });
          return false;  
     });
     /*
      *
      */
     if ($('.datepicker').length) {
         $('.datepicker').datetimepicker({
             locale: 'ru',
             format: 'DD.MM.YYYY',
         });
     }
     /*
      *
      */
     if ($('.timepicker').length) {
         $('.timepicker').datetimepicker({
             locale: 'ru',
             format: 'h:mm',
         });
     }
     /*
      *
      */
     $('.set-dt').on('click', function(){
         var self = $(this);
         var dt = self.data('date');
         self.parent().parent().find('input').val(dt);
         //$('input[name=start_date]').val(dt);
         return false;
     });
     /*
      *
      */
     $('#category').bind('change', function(){
          var self = $(this);
          $.post('/admin/subcat', {category: self.val()}, function(data){
               $('#model').html(data.result);
               $('.selectpicker').selectpicker('refresh');
          });
     });
     
     
     
});
//------------------------------------------------------
// Alerts
//------------------------------------------------------
Alerts = {
    message: function(type, title, message)
    {
	var msg = [];
	$.each(message, function(k, v){
	    
	    var arr = k.split('.');
	    var names = null;
	    if(arr.length == 2) {
		names = arr[0]+'['+arr[1]+']';
	    }
	    else if (arr.length == 3) {
		names = arr[0]+'['+arr[1]+']'+'['+arr[2]+']';
	    }
	    else
	    {
		names = arr[0];
	    }
	    
	    $('[name="'+names+'"]').parent().addClass('has-error');
	    $.each(v, function(k1, v1){
		msg.push('<li>'+v1+'</li>');
	    });
	});
	var text = '<ul style="padding:0">'+msg.join('')+'</ul>';
	   
	$.Notification.autoHideNotify(type,'top center', title, text);
	//$.Notification.notify(type,'top center', title, text);
	       
	$('form input, form select, form textarea, form .btn').focus(function() {
	    $(this).parent().removeClass('has-error');
	});
	
	Alerts.hide();
    },
    hide: function()
    {
	setTimeout(function(){
            $('.alert-danger').fadeOut(500);
            $('.alert-success').fadeOut(500);
        }, 5000);
    }
};
/*
 *
 */
function includeSryle(styleUrl) {
    $('head').append('<link rel="stylesheet" href="'+ styleUrl +'" type="text/css" />');
}
function include(scriptUrl) {
    document.write('<script src="' + scriptUrl + '"></script>');
}
/*
 * Маска телефона
 */
;
(function ($) {
    if ($('.mask-telefon').length) {
          include('/themes/libs/jquery.maskedinput.min.js');
          $(document).ready(function () {
               $('.mask-telefon').mask('+7(999) 999-99-99');
          });
    }
})(jQuery);
/*
 * Дата
 */
;
(function ($) {
    if ($('.dates').length) {
        include('/themes/site/assets/lib/moment/min/moment-with-locales.min.js');
        include('/themes/site/assets/lib/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js');
        includeSryle('/themes/site/assets/lib/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css');
        $(document).ready(function () {
            $('.dates').datetimepicker({
                locale: 'ru'    
            });
        });
    }
})(jQuery);
/*
 * Редактор
 */
;
(function ($) {
    if ($('.summernote').length) {
        include('/themes/admin/assets/assets/summernote/summernote.min.js');
        includeSryle('/themes/admin/assets/assets/summernote/summernote.css');
        $(document).ready(function () {
            $('.summernote').summernote({
                height: 200,                
                minHeight: null,             
                maxHeight: null,             
                focus: true                
            });
        });
    }
})(jQuery);
/*
 * Загрузка файлов
 */
;
(function ($) {
    if ($('#avatar-1').length) {
        $(document).ready(function () {
            
            $("#avatar-1").fileinput({
                overwriteInitial: true,
                maxFileSize: 1500,
                showClose: false,
                showCaption: false,
                browseLabel: '',
                removeLabel: '',
                browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
                removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
                removeTitle: 'Cancel or reset changes',
                elErrorContainer: '#kv-avatar-errors-1',
                msgErrorClass: 'alert alert-block alert-danger',
                defaultPreviewContent: '<img src="/uploads/avatar/'+avatar+'" alt="Ваш аватар" style="width:180px">',
                layoutTemplates: {main2: '{preview} {remove} {browse}'},
                allowedFileExtensions: ["jpg", "png", "gif", "jpeg"]
            });
            
        });
    }
})(jQuery);
/*
 * Срок действия
 */
;
(function ($) {
    if ($('.validity').length) {
        include('/themes/site/assets/lib/moment/min/moment-with-locales.min.js');
        include('/themes/site/assets/lib/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js');
        includeSryle('/themes/site/assets/lib/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css');
        $(document).ready(function () {
            $('.validity').datetimepicker({
                locale: 'ru',
                //minDate: new Date(),
                //maxDate: endDay,
                //date: endDay,
                //autoclose: true,
            }).on('dp.change', function(){
                $('.validity').data("DateTimePicker").hide();
            });
        });
    }
})(jQuery);
/*
 * Просмотр фото
 */
;
(function ($) {
     if ($('.image-popup').length) {
          include('/themes/admin/assets/assets/magnific-popup/magnific-popup.js');
          includeSryle('/themes/admin/assets/assets/magnific-popup/magnific-popup.css');
          $(document).ready(function () {
               $('.image-popup').magnificPopup({
                    type: 'image',
                    closeOnContentClick: true,
                    mainClass: 'mfp-fade',
                    gallery: {
                         enabled: true,
                         navigateByImgClick: true,
                         preload: [0,1] // Will preload 0 - before current, and 1 after the current image
                    }
               });
          });
     }
})(jQuery);



