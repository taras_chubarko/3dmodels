@if ( Session::has('errors') )
   <script>
        var text = '<ul style="padding:0">'
            @foreach ($errors->all() as $error)
                +'<li>{!! $error !!}</li>'
            @endforeach
        +'</ul>';
        $.Notification.notify('error','top center', 'Ошибка!', text);
   </script>
@endif

@if ( Session::has('success') )
    <script>
        $.Notification.notify('success','top center', 'Отлично!', '{!! Session::get('success') !!}');
    </script>
@endif

@if ( Session::has('warning') )
    <script>
        $.Notification.notify('warning','top center', 'Внимание!', '{!! Session::get('warning') !!}');
    </script>
@endif

@if ( Session::has('error') )
    <script>
        $.Notification.notify('error','top center', 'Ошибка!', '{!! Session::get('error') !!}');
    </script>
@endif

@if ( Session::has('info') )
    <script>
        $.Notification.notify('info','top center', 'Внимание!', '{!! Session::get('info') !!}');
    </script>
@endif

