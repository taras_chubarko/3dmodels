@extends('admin::layouts.master')

@section('content')

<div class="row">
    <div class="col-sm-12">
        <h4 class="pull-left page-title">{{ MetaTag::set('title', 'Создать блок') }}</h4>
        <ol class="breadcrumb pull-right">
            <li><a href="/admin">Админка</a></li>
            <li><a href="#">Структура</a></li>
            <li><a href="/admin/block">Блоки</a></li>
            <li class="active">Создать блок</li>
        </ol>
    </div>
</div>
<div class="row">
    <!-- Basic example -->
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                {!! Form::open(array('route' => 'admin.block.store', 'role' => 'form', 'class' => 'form')) !!}
                    
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <label for="name">Название</label>
                        {!! Form::text('name', null, array('class' => 'form-control')) !!}
                    </div>
                    
                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        <label for="title">Заголовок</label>
                        {!! Form::text('title', null, array('class' => 'form-control')) !!}
                    </div>
                        
                    <div class="form-group{{ $errors->has('body') ? ' has-error' : '' }}">
                        <label for="body">Содержимое</label>
                        {!! Form::textarea('body', null, array('class' => 'form-control editor', 'rows' => 10)) !!}
                    </div>
                        
                    <div class="form-group">
                        {!! Form::hidden('vid', 0) !!}
                        <div class="checkbox checkbox-primary">
                            {!! Form::checkbox('vid', 1, false, ['id' => 'vid']) !!}
                            <label for="vid">
                                Темный фон
                            </label>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        {!! Form::hidden('status', 0) !!}
                        <div class="checkbox checkbox-primary">
                            {!! Form::checkbox('status', 1, false, ['id' => 'status']) !!}
                            <label for="status">
                                Активный
                            </label>
                        </div>
                    </div>
                        
                    <div class="form-group">
                       {!! Form::submit('Сохранить', array('class' => 'btn btn-primary btn-flat')) !!}
                    </div>
                {!! Form::close() !!}
            </div>
            <!-- panel-body -->
        </div>
        <!-- panel -->
    </div>
    <!-- col-->
</div>

@stop
