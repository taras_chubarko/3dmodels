<div class="uploader-item">
    <a class="img-preview" href="" data-fancybox-group="gallery">
        <img src="{{ config('app.link') }}/filem/image/{{ $uri }}/{{ $actions }}/{{ $image->filename }}" alt="">
        {!! Form::hidden($name.'[fid]['.$image->id.']', $image->id) !!}
    </a>
    <a href="{{ route('filem.destroy.id', $image->id) }}" class="img-delete">×</a>
</div>