<div id="image-widget">
    @if($image)
        @include('admin::filem.image', ['name' => $name, 'uri' => $uri, 'actions' => $actions, 'image' => $image])
    @endif
</div>

<input id="image-field" type="file" class="file-loading">

@section('style')
    @parent
    <link href="/themes/libs/bootstrap-fileinput/css/fileinput.min.css" rel="stylesheet" type="text/css" />
@stop

@section('script')
    @parent
    <script src="/themes/libs/bootstrap-fileinput/js/fileinput.min.js"></script>
    <script src="/themes/libs/bootstrap-fileinput/js/locales/ru.js"></script> 
    @include('admin::filem.script-image', ['name' => $name, 'uri' => $uri, 'actions' => $actions])
@stop