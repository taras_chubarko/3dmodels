<div class="image image-{{ $actions }}">
    <a class="text-danger image-delete" href="{{ route('filem.destroy.id', $image->id) }}"><i class="fa fa-times-circle" aria-hidden="true"></i></a>
    <img src="{{ config('app.link') }}/filem/image/{{ $uri }}/{{ $actions }}/{{ $image->filename }}" class="img-responsive" alt="{{ $image->original }}">
    {!! Form::hidden($name.'[fid]', $image->id) !!}
</div>