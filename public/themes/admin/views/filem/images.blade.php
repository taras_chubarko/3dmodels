<div id="images-widget">
    @if($images)
        @foreach($images as $image)
            @include('admin::filem.im', ['image' => $image, 'uri' => $uri, 'actions' => $actions])
        @endforeach
    @endif
</div>
<input id="images-field" type="file" class="file-loading" multiple>


@section('style')
    @parent
    <link href="/themes/libs/bootstrap-fileinput/css/fileinput.min.css" rel="stylesheet" type="text/css" />
@stop

@section('script')
    @parent
    <script src="/themes/libs/bootstrap-fileinput/js/fileinput.min.js"></script>
    <script src="/themes/libs/bootstrap-fileinput/js/locales/ru.js"></script> 
    @include('admin::filem.script-images', ['name' => $name, 'uri' => $uri, 'actions' => $actions])
@stop