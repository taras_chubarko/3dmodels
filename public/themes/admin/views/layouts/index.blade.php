@extends('admin::layouts.master')

@section('content') 
<!-- Page-Title -->
<div class="row">
    <div class="col-sm-12">
        <!--'1iDUuQJFMoffO2wHSbV5' => 'Admin',-->
        <h4 class="pull-left page-title">Панель управления</h4>
        <ol class="breadcrumb pull-right">
            <li><a href="/admin">Панель управления</a></li>
           <!-- 'UlVc2CfPIDn9inW4zHxk' => 'Statistics',-->
            <li class="active">Панель управления</li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-md-6 col-sm-6 col-lg-3">
        <div class="mini-stat clearfix bx-shadow">
            <span class="mini-stat-icon bg-success"><i class="fa fa-tasks" aria-hidden="true"></i></span>
            <div class="mini-stat-info text-right text-muted">
                <span class="counter">0</span>
                что нибуть
            </div>
            <div class="tiles-progress">
                <div class="m-t-20">
                    <h5 class="text-uppercase"><a href="#!">что нибуть</a></h5>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-lg-3">
        <div class="mini-stat clearfix bx-shadow">
            <span class="mini-stat-icon bg-info"><i class="fa fa-comments-o" aria-hidden="true"></i></span>
            <div class="mini-stat-info text-right text-muted">
                <span class="counter">0</span>
                что нибуть
            </div>
            <div class="tiles-progress">
                <div class="m-t-20">
                    <h5 class="text-uppercase"><a href="#!">что нибуть</a></h5>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-lg-3">
        <div class="mini-stat clearfix bx-shadow">
            <span class="mini-stat-icon bg-purple"><i class="fa fa-money" aria-hidden="true"></i></span>
            <div class="mini-stat-info text-right text-muted">
                <span class="counter">0 <small> руб.</small></span>
                что нибуть
            </div>
            <div class="tiles-progress">
                <div class="m-t-20">
                    <h5 class="text-uppercase"><a href="#!">что нибуть</a></h5>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6 col-lg-3">
        <div class="mini-stat clearfix bx-shadow">
            <span class="mini-stat-icon bg-primary"><i class="ion-android-contacts"></i></span>
            <div class="mini-stat-info text-right text-muted">
                <span class="counter">{{ User::all()->count() }}</span>
                <!--'sLn1xg3XeS9in2XFOCKZ' => 'All users',-->
                Всего
            </div>
            <div class="tiles-progress">
                <div class="m-t-20">
                    <!--'pD6LrRHBV8MUgnPwg91U' => 'Users',-->
                    <h5 class="text-uppercase"><a href="#!">Пользователи</a></h5>
                </div>
            </div>
        </div>
    </div>
</div>


@stop