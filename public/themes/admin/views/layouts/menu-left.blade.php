<ul class="sidebar-menu">
    <li>
        <!--'CX7v3BUdGZ3RyJO9BLxM' => 'Control panel',-->
        <a class="waves-effect active" href="/admin"><i class="md md-home"></i><span> Панель управления</span></a>
    </li>
    <li class="has_sub">
        <!--'dcK2nvj8KhbpMRbZBjCZ' => 'Content',-->
        <a class="waves-effect" href="#!"><i class="fa fa-file-text"></i> <span>Содержимое</span><span class="pull-right"><i class="md md-add"></i></span></a>
        <ul class="treeview-menu">
            <!--<li><a href="{{-- route('admin.faq.index') --}}"><span>Вопросы-ответы</span></a></li>
            <li><a href="{{-- route('admin.news.index') --}}"><span>Новости</span></a></li>-->
        </ul>
    </li>
    
    <li class="has_sub">
        <!--'yZTG3VvGKNPMLnXKydn5' => 'Structure',-->
        <a class="waves-effect" href="#"><i class="fa fa-cube"></i> <span>Структура</span><span class="pull-right"><i class="md md-add"></i></span></a>
        <ul class="treeview-menu">
            <li><a href="{{ route('admin.taxonomy.index') }}"><span>Словари</span></a></li>
        </ul>
    </li>
    <!--'pD6LrRHBV8MUgnPwg91U' => 'Users',-->
    <li><a href="{{ route('admin.users.index') }}"><i class="fa fa-users"></i> <span>Пользователи</span></a></li>
    
</ul>