<div class="panel-group" id="meta">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4 class="panel-title">
                <a data-toggle="collapse" data-parent="#meta" href="#collapseOne">
                    Мета-теги
                </a>
            </h4>
        </div>
        <div id="collapseOne" class="panel-collapse collapse">
            <div class="panel-body">
                <div class="form-group">
                    <label for="title">Мета - title</label>
                    {!! Form::text('meta[title]', ($meta) ? $meta->title : null, array('class' => 'form-control')) !!}
                </div>
                  
                <div class="form-group">
                    <label for="keywords">Мета - keywords</label>
                    {!! Form::text('meta[keywords]', ($meta) ? $meta->keywords : null, array('class' => 'form-control')) !!}
                </div>
                  
                <div class="form-group">
                    <label for="description">Мета - description</label>
                    {!! Form::textarea('meta[description]', ($meta) ? $meta->description : null, array('class' => 'form-control', 'rows' => 5)) !!}
                </div>
                    
                <div class="form-group">
                    <label for="robots">Индексация</label>
                    {!! Form::select('meta[robots]', config('meta.robots'), ($meta) ? $meta->robots : null, array('class' => 'form-control selectpicker', 'title' => '- Выбрать -')) !!}
                </div>
                    
                {!! Form::hidden('meta[entity]', $entity) !!}
                {!! Form::hidden('meta[entity_id]', $entity_id) !!}
            </div>
        </div>
    </div>
</div>