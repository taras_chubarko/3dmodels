@extends('admin::layouts.master')

@section('content')
    
<div class="row">
    <div class="col-sm-12">
        <h4 class="pull-left page-title">{{ MetaTag::set('title', 'Мета теги') }}</h4>
        <ol class="breadcrumb pull-right">
            <li><a href="/admin">Админка</a></li>
            <li><a href="#">Настройки</a></li>
            <li class="active">Мета теги</li>
        </ol>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <a href="{{ route('admin.meta.create') }}" class="btn btn-success waves-effect waves-light m-b-20" id="addToTable">Создать <i class="fa fa-plus"></i></a>
                <br>
                @include('admin::meta.form-filter')
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        @if($meta->count())
                        <table id="datatable-editable" class="table table-bordered table-striped dataTable no-footer">
                            <thead>
                                <tr>
                                    <th width="20">#</th>
                                    <th>Тип материала</th>
                                    <th>Ключ материала</th>
                                    <th>Мета - title</th>
                                    <th>Дата</th>
                                    <th width="100">Действие</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($meta as $item)
                                <tr id="tr-{{ $item->id }}">
                                    <td>{{ $item->id }}</td>
                                    <td>{{ config('meta.entity.'.$item->entity) }}</td>
                                    <td>{{ $item->entity_id }}</td>
                                    <td>{{ $item->title }}</td> 
                                    <td>{{ $item->created_at->format('d.m.Y') }}</td>
                                    <td class="actions">
                                        <a class="on-default edit-row" href="{{ route('admin.meta.edit', $item->id) }}"><i class="fa fa-pencil"></i></a>
                                        <a class="on-default remove-row sa-warning" href="{{ route('admin.meta.destroy', $item->id) }}" data-msg-error="Вы не сможете восстановить этот тег!" data-msg-success="Ваш тег удален."><i class="fa fa-trash-o"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {!! $meta->render() !!}
                        @else
                        <p>Нет мета тегов.</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop