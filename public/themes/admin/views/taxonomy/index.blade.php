@extends('admin::layouts.master')

@section('content')
	<div class="row">
		<div class="col-sm-12">
			<h4 class="pull-left page-title">{{ MetaTag::set('title', 'Словари таксономии') }}</h4>
			<ol class="breadcrumb pull-right">
				<li><a href="/admin">Админка</a></li>
				<li><a href="#">Структура</a></li>
				<li class="active">Словари таксономии</li>
			</ol>
		</div>
	</div>	
	
	<div class="row">
		<div class="col-md-12">
		    <div class="panel panel-default">
			<div class="panel-heading">
			    <a href="{{ route('admin.taxonomy.create') }}" class="btn btn-primary waves-effect waves-light" id="addToTable">Создать <i class="fa fa-plus"></i></a>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						@if($taxonomy->count())
						<table id="datatable-editable" class="table table-bordered table-striped dataTable no-footer">
							<thead>
								<tr>
									<th width="20">#</th>
									<th>Название</th>
									<th width="50">Действия</th>
								</tr>
							</thead>
							<tbody>
								@foreach($taxonomy as $item)
								<tr>
									<td>{{ $item->id }}</td>
									<td>
										@if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('taxonomy.create'))
											<a href="{{ route('admin.taxonomy.show', $item->id) }}">{{ $item->name }}</a>
										@else
											{{ $item->name }}
										@endif
									</td>
									<td class="actions">
										<a class="on-default edit-row" href="{{ route('admin.taxonomy.edit', $item->id) }}"><i class="fa fa-pencil"></i></a>
										<a class="on-default remove-row sa-warning" href="{{ route('admin.taxonomy.destroy', $item->id) }}" data-msg-error="Вы не сможете восстановить этот словарь!" data-msg-success="Ваш словарь удален."><i class="fa fa-trash-o"></i></a>
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
						{!! $taxonomy->render() !!}
						@else
							<p>Нет словарей.</p>
						@endif
					</div>
				</div>
			</div>
			
		    </div>
		</div>
	</div>

@stop