@extends('admin::layouts.master')

@section('content')

<div class="row">
    <div class="col-sm-12">
	<h4 class="pull-left page-title">{{ MetaTag::set('title', $taxonomy->name) }}</h4>
	<ol class="breadcrumb pull-right">
	    <li><a href="/admin">Админка</a></li>
	    <li><a href="#">Структура</a></li>
            <li><a href="/admin/taxonomy">Словари таксономии</a></li>
	    <li class="active">{{ $taxonomy->name }}</li>
	</ol>
    </div>
</div>
<div class="row">
    <!-- Basic example -->
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div id="nestable_list_menu" class="text-left">
                    <a href="{{ route('admin.term.create') }}?taxonomy_id={{ $taxonomy->id }}" class="btn btn-success btn-flat">Добавить термин</a>
                    <button data-action="expand-all" class="btn btn-pink waves-effect waves-light" type="button">Развернуть</button>
                    <button data-action="collapse-all" class="btn btn-purple waves-effect waves-light" type="button">Свернуть</button>
                </div>
            </div>
            <div class="panel-body">
                @if($taxonomy->terms->count())
                    <div id="nestable_list_3" class="dd" data-link="/admin/taxonomy/term/sort">
                        @include('admin::taxonomy.terms.lists', ['lists' => $taxonomy->terms->toHierarchy()])
                    </div>
                @else
                    <p>Нет терминов таксономии.</p>
                @endif
            </div>
            <!-- panel-body -->
        </div>
        <!-- panel -->
    </div>
    <!-- col-->
</div>

@stop

@section('style')
    <link href="/themes/admin/assets/assets/nestable/jquery.nestable.css" rel="stylesheet" type="text/css" />
@stop

@section('script')
    <script src="/themes/admin/assets/assets/nestable/jquery.nestable.js"></script>
    <script src="/themes/admin/assets/assets/nestable/nestable.js"></script>
@stop