@extends('admin::layouts.master')

@section('content')
    
<div class="row">
    <div class="col-sm-12">
	<h4 class="pull-left page-title">{{ MetaTag::set('title', 'Редактировать термин таксономии') }}</h4>
	<ol class="breadcrumb pull-right">
	    <li><a href="/admin">Админка</a></li>
	    <li><a href="#">Структура</a></li>
            <li><a href="/admin/taxonomy">Словари таксономии</a></li>
	    <li class="active">Редактировать термин таксономии</li>
	</ol>
    </div>
</div>
    
<div class="row">
    <!-- Basic example -->
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-body">
                {!! Form::open(array('route' => ['admin.term.update', $term->id], 'method' => 'PUT', 'role' => 'form', 'class' => 'form')) !!}
                
                    <div class="row">
			<div class="col-lg-4">
			    <div class="form-group{{ $errors->has('name.en') ? ' has-error' : '' }}">
				<label for="name">Name (en) *</label>
				{!! Form::text('name[en]', $term->fieldName['en'], array('class' => 'form-control')) !!}
			    </div>
			</div>
			<div class="col-lg-4">
			    <div class="form-group{{ $errors->has('name.ru') ? ' has-error' : '' }}">
				<label for="name">Название (ru) *</label>
				{!! Form::text('name[ru]', $term->fieldName['ru'], array('class' => 'form-control')) !!}
			    </div>
			</div>
			<div class="col-lg-4">
			    <div class="form-group{{ $errors->has('name.uk') ? ' has-error' : '' }}">
				<label for="name">Назва (uk) *</label>
				{!! Form::text('name[uk]', $term->fieldName['uk'], array('class' => 'form-control')) !!}
			    </div>
			</div>
		    </div>
			
                    <div class="form-group{{ $errors->has('parent_id') ? ' has-error' : '' }}">
                        <label for="parent_id">Родитель *</label>
                        {!! Form::select('parent_id', TaxonomyTerm::itemsArray($term->taxonomy_id), $term->parent_id, array('class' => 'form-control selectpicker')) !!}
                    </div>
			
		    <div class="form-group{{ $errors->has('ico') ? ' has-error' : '' }}">
                        <label for="ico">Клас иконки</label>
			{!! Form::text('ico', $term->ico, array('class' => 'form-control')) !!}
                    </div>
                          
                    <div class="form-group">
                        <label for="order">Позиция в списке</label>
                        {!! Form::selectRange('sort', 0, 50, $term->sort, array('class' => 'form-control selectpicker')) !!}
                    </div>
			
                    <div class="form-group">
                        {!! Form::hidden('taxonomy_id', $term->taxonomy_id) !!}
                        {!! Form::submit('Сохранить', array('class' => 'btn btn-primary btn-flat')) !!}
                    </div>
                    
                {!! Form::close() !!}
            </div>
            <!-- panel-body -->
        </div>
        <!-- panel -->
    </div>
    <!-- col-->
</div>

@stop