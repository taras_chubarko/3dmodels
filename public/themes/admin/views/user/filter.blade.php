<div id="panel-modal-filter" class="modal fade in" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content p-0 b-0">
            <div class="panel panel-color panel-primary">
                <div class="panel-heading">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button> 
                    <h3 class="panel-title">Фильтр</h3>
                </div>
                <div class="panel-body">
                    {!! Form::open(array('url' => Request::url(), 'role' => 'form', 'class' => 'form', 'method' => 'GET')) !!}
                        <div class="row">
                            <div class="col-lg-2">
                                <div class="form-group">
                                    <label for="name">ID</label>
                                    {!! Form::text('id', Request::get('id'), array('class' => 'form-control')) !!}
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <label for="email">Почта</label>
                                    {!! Form::email('email', Request::get('email'), array('class' => 'form-control')) !!}
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <label for="email">Город</label>
                                    {!! Form::text('location', null, array('class' => 'form-control locaton')) !!}
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 text-right">
                                {!! Form::submit('Применить', array('class' => 'btn btn-primary')) !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>