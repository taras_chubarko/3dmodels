@extends('admin::layouts.master')

@section('content')

<div class="row">
    <div class="col-sm-12">
	<h4 class="pull-left page-title">{{ MetaTag::set('title', 'Пользователи') }}</h4>
	<ol class="breadcrumb pull-right">
	    <li><a href="/admin">Панель управления</a></li>
	    <li><a href="#">Пользователи</a></li>
	    <li class="active">Пользователи</li>
	</ol>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                @if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('users.create'))
                    <div class="dropdown">
			<a href="{{ route('admin.users.create') }}" class="btn btn-success waves-effect waves-light" id="addToTable">Добавить <i class="fa fa-plus"></i></a><!--'jy6aQ3CiOSPMuUSsE3mv' => 'Create',-->
		    </div>
		@endif
               <!-- include('user::admin.users.tabs')
                include('user::admin.users.filter')-->
            </div>
            <div class="panel-body">
               <!-- include('admin::user.tabs')-->
		<div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        @if($users->count())
                        <table id="datatable-editable" class="table table-bordered table-striped dataTable no-footer">
                            <thead>
                                <tr>
                                    <th width="10">ID</th>
                                    <th>Имя</th><!--'JzsJGTP2NCfTzwbfQWon' => 'Name',-->
                                    <th>E-mail</th><!--'ruEePqlTYPZgYpcpz2hh' => 'E-mail',-->
                                    <th width="150">Дата регистрации</th><!--'EULYqM6OIu1kIIGuZSrO' => 'Registration date',-->
                                    <th width="250">Роль</th><!--'t1ymiFJKozliWDCi93BC' => 'Role',-->
                                    <th width="120">Статус</th><!--'uOydCMrdWA8h5z2uwVzv' => 'Status',-->
                                    <th width="100">Действия</th><!--'QXzVR2s2qUTtwtejw7BH' => 'Action',-->
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->fullName }}</td>
                                    <td>{{ $item->email }}</td>
                                    <td>{{ $item->created_at->format('d.m.Y') }}</td>
                                    <td>{{ $item->userRoles }}</td>
                                    <td>{!! $item->flag !!}</td>
                                    <td>
					
                                        @if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('users.edit'))
                                            <a class="on-default edit-row m-r-10" href="{{ route('admin.users.edit', $item->id) }}"><i class="fa fa-pencil"></i></a>
                                        @endif
                                        @if(Sentinel::getUser()->inRole('admin') || Sentinel::getUser()->hasAccess('users.delete'))
                                            <a class="on-default remove-row sa-warning m-r-10" href="{{ route('admin.users.destroy', $item->id) }}" data-msg-error="Что хотите удалить пользователя" data-msg-success="Пользователь удален!"><i class="fa fa-trash-o"></i></a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {!! $users->render() !!}
                        @else
                            <p>Нет пользователей.</p>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('admin::user.filter')
@stop