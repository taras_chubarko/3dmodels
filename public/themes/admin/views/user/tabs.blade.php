<ul class="nav nav-tabs navtab-bg">
    <li class="{{ active_route(route('admin.users.customer')) }}"> 
        <a href="{{ route('admin.users.customer') }}"> 
            <span>Заказчик</span> 
        </a> 
    </li>
    <li class="{{ active_route(route('admin.users.executor')) }}"> 
        <a href="{{ route('admin.users.executor') }}"> 
            <span>Исполнитель</span> 
        </a> 
    </li>
    <li class="{{ active_route(route('admin.users.moderator')) }}"> 
        <a href="{{ route('admin.users.moderator') }}"> 
            <span>Модератор</span> 
        </a> 
    </li>
    <li class="{{ active_route(route('admin.users.admin')) }}"> 
        <a href="{{ route('admin.users.admin') }}"> 
            <span>Администратор</span> 
        </a> 
    </li>
    <li class="{{ active_route(route('admin.zajavka.index')) }}"> 
        <a href="{{ route('admin.zajavka.index') }}"> 
            <span>Заявки на роли</span> 
        </a> 
    </li>
</ul>