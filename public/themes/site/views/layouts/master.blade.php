<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>{{ config('app.name') }}</title>
    <meta content="{{ MetaTag::get('description') }}" name="description" id="description">
    <meta content="{{ MetaTag::get('keywords') }}" name="keywords" id="keywords">
    {!! MetaTag::openGraph() !!}
    {!! MetaTag::twitterCard() !!}
    <link href="/favicon.ico" type="image/x-icon" rel="icon" />
    <link href="{{ mix('/themes/site/assets/css/app.css') }}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>
{{-- dd(Request::segment(1)) --}}
    <div id="app">
        <div class="wrapper">
            <header class="header" v-bind:class="headerClass">
                <header-top></header-top>
                <header-center></header-center>
                <header-catalog></header-catalog>
                <component :is="currentBlock"></component>
            </header>
            <!-- .header-->
            <div class="content">
                @include('site::alert.alert')
                <router-view></router-view>
                <vue-progress-bar></vue-progress-bar>
                <notifications classes="cp"/>
            </div>
            <!-- .content -->
        </div>
        <!-- .wrapper -->
        <footer class="footer">
            <footers></footers>
        </footer>
        <!-- .footer -->
    </div>
    <script src="{{ mix('/themes/site/assets/js/app.js') }}"></script>
    <script src="//ulogin.ru/js/ulogin.js?lang=en"></script>
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.js"></script>-->
</body>

</html>
