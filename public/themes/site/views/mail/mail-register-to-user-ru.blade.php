@extends('site::mail.main')

@section('title')
Спасибо за регистрацию!
@stop

@section('content')
    <p>Здравствуйте!</p>
    <br>
    <p>Поздравляем с успешной регистрацией на сайте {{ config('app.name') }}.</p>
    <br>
    <p>Ваш E-mail: {{ $user->email }}</p>
    @if(!empty($request->password))
        <p>Ваш пароль: {{ $request->password }}</p>
    @endif
    <br>
@stop