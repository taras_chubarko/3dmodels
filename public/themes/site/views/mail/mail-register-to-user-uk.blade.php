@extends('site::mail.main')

@section('title')
Дякую за реєстрацію!
@stop

@section('content')
    <p>Привіт!</p>
    <br>
    <p>Вітаємо з успішною реєстрацією на сайті {{ config('app.name') }}.</p>
    <br>
    <p>Ваш E-mail: {{ $user->email }}</p>
    @if(!empty($request->password))
        <p>Ваш пароль: {{ $request->password }}</p>
    @endif
    <br>
@stop