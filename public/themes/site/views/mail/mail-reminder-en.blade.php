@extends('site::mail.main')

@section('title')
Password recovery
@stop

@section('content')
    <p>Hello!</p>
    <br>
    <p>To recover the password on the site {{ config('app.name') }}, enter the code in the appropriate field in the recovery form.</p>
    <br>
    <p>Your code: {{ $reminder->code }}</p>
    <br>
@stop