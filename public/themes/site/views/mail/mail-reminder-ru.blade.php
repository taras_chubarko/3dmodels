@extends('site::mail.main')

@section('title')
Восстановление пароля
@stop

@section('content')
    <p>Привет!</p>
    <br>
    <p>Для восстановления пароля на сайте {{ config('app.name') }}, введите код в соответствующее поле в форме восстановления.</p>
    <br>
    <p>Ваш код: {{ $reminder->code }}</p>
    <br>
@stop