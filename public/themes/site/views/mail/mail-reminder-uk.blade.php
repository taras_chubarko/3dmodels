@extends('site::mail.main')

@section('title')
Відновлення паролю
@stop

@section('content')
    <p>Привіт!</p>
    <br>
    <p>Для відновлення паролю на сайті {{ config('app.name') }}, введіть код в відповідне поле в формі відновлення.</p>
    <br>
    <p>Ваш код: {{ $reminder->code }}</p>
    <br>
@stop