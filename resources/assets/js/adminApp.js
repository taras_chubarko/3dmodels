require('./bootstrap');

window.Vue = require('vue');

import VueSweetAlert from 'vue-sweetalert';
Vue.use(VueSweetAlert);


Vue.component('upload-foto', require('./components/admin/UploadFoto.vue'));
Vue.component('select-filters', require('./components/admin/SelectFilters.vue'));


/*
 * App
 */
const app = new Vue({
    el: '#wrapper',
    data: {
        
    },
    mounted() {
        console.log('admin panel');
    }
});