
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');
window.jsonp = require('jsonp');

import VueRouter from 'vue-router';
import * as VueGoogleMaps from 'vue2-google-maps';
import VueMask from 'v-mask';
import VeeValidate from 'vee-validate';
import ru from 'vee-validate/dist/locale/ru';
import en from 'vee-validate/dist/locale/en';
import uk from 'vee-validate/dist/locale/uk';
import VueScrollTo from 'vue-scrollto';
import VueSweetAlert from 'vue-sweetalert';
import VueProgressBar from 'vue-progressbar';
import VN  from 'vue-notification';
import Msg from './packages/validate/Msg.js';
import Auth from './packages/auth/Auth.js';
import Block from './packages/Block.js';
import VueI18n from 'vue-i18n';

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

/*
 * use
 */
Vue.use(VueRouter);
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyBvWE_sIwKbWkiuJQOf8gSk9qzpO96fhfY',
    libraries: 'places', // This is required if you use the Autocomplete plugin 
    // OR: libraries: 'places,drawing' 
    // OR: libraries: 'places,drawing,visualization' 
    // (as you require) 
  }
});
Vue.use(VueMask);

VeeValidate.Validator.addLocale(ru);
VeeValidate.Validator.addLocale(en);
VeeValidate.Validator.addLocale(uk);
Vue.use(VeeValidate, {
    inject: false,
    locale: 'ru',
});

Vue.use(VueScrollTo);
Vue.use(VueSweetAlert);
Vue.use(VN);
Vue.use(Msg);
Vue.use(VueProgressBar, {
  color: '#34A853',
  failedColor: '#EA4335',
  thickness: '5px'
});
Vue.use(Auth);
Vue.use(VueI18n);
Vue.use(Block);

Vue.use(require('vue-faker'));

import AnimatedVue from 'animated-vue';
Vue.use(AnimatedVue);

import VueHead from 'vue-head';
Vue.use(VueHead);


//var cors = require('cors');
//Vue.use(cors);

//import VueSocketio from 'vue-socket.io';
//Vue.use(VueSocketio, 'http://3dmodels.progim.net:3000');


//import VueSocketio from 'vue-socket.io';
//var client = require('socket.io-client');
//var socketServer = 'http://3dmodels.progim.net:3000/socket.io';
//var options = {
//  autoConnect: false
//};
//
//Vue.use(VueSocketio, client(socketServer, options));


import {VueMasonryPlugin} from 'vue-masonry';
Vue.use(VueMasonryPlugin);


export default {
  provideValidator: true,
  inject: ['$validator']
}
/*
 * Components
 */
Vue.component('header-top', require('./components/layouts/HeaderTop.vue'));
Vue.component('header-center', require('./components/layouts/HeaderCenter.vue'));
Vue.component('header-catalog', require('./components/layouts/HeaderCatalog.vue'));
Vue.component('header-slot', require('./components/layouts/HeaderSlot.vue'));
Vue.component('footers', require('./components/layouts/Footer.vue'));
//Blocks
Vue.component('change-lang', require('./components/blocks/ChangeLang.vue'));
Vue.component('block-find', require('./components/blocks/Find.vue'));
Vue.component('block-find-with-photo', require('./components/blocks/FindWithPhoto.vue'));
Vue.component('block-top', require('./components/blocks/BlockTop.vue'));
Vue.component('block-new', require('./components/blocks/BlockNew.vue'));
Vue.component('block-group-week', require('./components/blocks/BlockGroupWeek.vue'));
Vue.component('block-gall', require('./components/blocks/BlockGall.vue'));
Vue.component('block-work', require('./components/blocks/BlockWork.vue'));
Vue.component('block-login', require('./components/blocks/BlockLogin.vue'));
Vue.component('block-user', require('./components/blocks/BlockUser.vue'));
Vue.component('block-mess', require('./components/blocks/BlockMess.vue'));
Vue.component('block-notify', require('./components/blocks/BlockNotify.vue'));
Vue.component('block-cart', require('./components/blocks/BlockCart.vue'));
Vue.component('block-string', require('./components/blocks/BlockString.vue'));
Vue.component('block-profile-my', require('./components/user/BlockProfileMy.vue'));
Vue.component('menu-profile-my', require('./components/user/MenuProfile.vue'));
Vue.component('menu-aktyvnist', require('./components/user/MenuAktyvnist.vue'));
//
const Main = Vue.extend(require('./components/pages/Main.vue'));
const Page404 = Vue.extend(require('./components/pages/Page404.vue'));
const Login = Vue.extend(require('./components/pages/Login.vue'));
const Logout = Vue.extend(require('./components/pages/Logout.vue'));
// user profile
const PageProfile = Vue.extend(require('./components/user/PageProfile.vue'));
const PageAllNotifications = Vue.extend(require('./components/user/PageAllNotifications.vue'));
const PageNalashtuvannya = Vue.extend(require('./components/user/PageNalashtuvannya.vue'));
const PageDruzi = Vue.extend(require('./components/user/PageDruzi.vue'));
const PagePovidomlennya = Vue.extend(require('./components/user/PagePovidomlennya.vue'));
const PageHalereya = Vue.extend(require('./components/user/PageHalereya.vue'));
const PageProdukty = Vue.extend(require('./components/user/PageProdukty.vue'));
const PageProdazhPokupka = Vue.extend(require('./components/user/PageProdazhPokupka.vue'));
const PageZakladky = Vue.extend(require('./components/user/PageZakladky.vue'));
const PageTaryfnyyPlan = Vue.extend(require('./components/user/PageTaryfnyyPlan.vue'));
/*
 * Router
 */
const router = new VueRouter({
  mode: 'history',
  routes: [
    { path: '/', component: Main },
    {
        path: '/login',
        component: Login,
        meta: {
            forGuests: true
        }
    },
    {
        path: '/profile/activity',
        component: PageProfile,
        meta: {
            forUser: true
        }
    },
    {
        path: '/profile/activity/all-notifications',
        component: PageAllNotifications,
        meta: {
            forUser: true
        }
    },
    {
        path: '/profile/settings',
        component: PageNalashtuvannya,
        meta: {
            forUser: true
        }
    },
    {
        path: '/profile/friends',
        component: PageDruzi,
        meta: {
            forUser: true
        }
    },
    {
        path: '/profile/message',
        component: PagePovidomlennya,
        meta: {
            forUser: true
        }
    },
    {
        path: '/profile/gallery',
        component: PageHalereya,
        meta: {
            forUser: true
        }
    },
    {
        path: '/profile/products',
        component: PageProdukty,
        meta: {
            forUser: true
        }
    },
    {
        path: '/profile/sale-buy',
        component: PageProdazhPokupka,
        meta: {
            forUser: true
        }
    },
    {
        path: '/profile/bookmarks',
        component: PageZakladky,
        meta: {
            forUser: true
        }
    },
    {
        path: '/profile/tariff-plan',
        component: PageTaryfnyyPlan,
        meta: {
            forUser: true
        }
    },
    
    
    { path: '/logout', component: Logout },
   // { path: '/*', component: Page404 },
    
  ]
});


router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.forGuests)) {
        if (Vue.auth.isAuth()) {
            next({
                path: '/'
            });
        }
        else
        {
            next();
        }
    }
    else if (to.matched.some(record => record.meta.forOrder)) {
        var order = localStorage.getItem('order');
        if (order) {
          next();
        }
        else
        {
            next({
                path: '/'
            });
        }
        //if (!Vue.auth.isAuth()) {
        //    next({
        //        path: '/login'
        //    });
        //}
        //else
        //{
        //  next();
        //}
    }
    else if (to.matched.some(record => record.meta.forUser)) {
        if (!Vue.auth.isAuth()) {
            next({
                path: '/'
            });
        }
        else
        {
            next();
        }
    }
    else
    {
        next();
    }
});

//router.beforeEach(function() {  
//  window.scrollTo(0, 0);
//});
Vue.filter('newstring', function (value) {
    return value.replace(/(?:\r\n|\r|\n)/g, '<br />');
});

import ens from './locales/en.js';
import rus from './locales/ru.js';
import uks from './locales/uk.js';

var messages = {
  'uk' : uks,
  'ru' : rus,
  'en' : ens,
}


const i18n = new VueI18n({
  locale: (localStorage.getItem('currentLang')) ? localStorage.getItem('currentLang') : 'uk', // set locale
  fallbackLocale: 'en',
  messages, // set locale messages
});

Vue.prototype.$locale = {
    change (lang) {
	i18n.locale = lang;
    },
    current() {
	return i18n.locale;
    }
}

import { EventBus } from './components/event-bus.js';
/*
 * filters
 */
Vue.filter('Vidhuk', function(val) {
    if (val == 0) {
        return '<span>'+val+'</span> '+i18n.t('Vidhukiv');
    }
    else if (val == 1) {
       return '<span>'+val+'</span> '+i18n.t('Vidhuk');
    }
    else if (val == 2 || val == 3 || val == 4) {
        return '<span>'+val+'</span> '+i18n.t('Vidhuky');
    }
    else if (val >= 5) {
        return '<span>'+val+'</span> '+i18n.t('Vidhukiv');
    }
    else
    {
        return '<span>'+val+'</span> '+i18n.t('Vidhukiv');
    }
});

require('./filters');
//var socket = io();
//console.log(socket);
import VueSocketio from 'vue-socket.io';
Vue.use(VueSocketio, 'http://metinseylan.com:1992');




/*
 * App
 */
const app = new Vue({
    el: '#app',
    router:router,
    i18n: i18n,
    data: {
      sitename: '3D Models',
      currentBlock: 'block-find',
      headerClass: 'front-header',
    },
    methods:{
        
    },
    mounted() {
        //localStorage.setItem('currentLang', 'укр');
        var lng = localStorage.getItem('currentLang');
        if (!lng) {
            localStorage.setItem('currentLang', 'uk');
        }
        this.$auth.whatch();
        
        
    }
});
