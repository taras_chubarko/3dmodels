export default {
    month: {
        name: 'На месяц',
        value: '200',
        bulets: {
            0:{
                name: '1 ГБ',
            },
            1:{
                name: '2 ГБ',
            },
            2:{
                name: '3 ГБ',
            },
            3:{
                name: '∞',
            }
        }
    },
    year: {
        name: 'На год',
        value: '1 200',
        bulets: {
            0:{
                name: '10 ГБ',
            },
            1:{
                name: '20 ГБ',
            },
            2:{
                name: '30 ГБ',
            },
            3:{
                name: '∞',
            }
        }
    }
}