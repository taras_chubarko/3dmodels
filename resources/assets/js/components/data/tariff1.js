export default {
    intro: {
        name: 'Интро',
        blocks: {
            0: {
                name: 'Аб. плата',
                value: '490 руб',
                color: '#FF8463',
                width: '20%',
                height: '19%'
            },
            1: {
                name: 'На Билайн России',
                value: '∞ мин',
                color: '#FF6688',
                width: '20%',
                height: '100%'
            },
            2: {
                name: 'Интернет',
                value: '3 ГБ',
                color: '#992C2F',
                width: '20%',
                height: '7.5%'
            },
            3: {
                name: 'Местных минут',
                value: '500 мин',
                color: '#7BA863',
                width: '20%',
                height: '7.5%'
            },
            4: {
                name: 'СМС',
                value: '∞',
                color: '#D9BC4C',
                width: '20%',
                height: '100%'
            }
        },
    },
    mini: {
        name: 'Мини',
        blocks: {
            0: {
                name: 'Аб. плата',
                value: '790 руб',
                color: '#FF8463',
                width: '16.6667%',
                height: '30.5019%'
            },
            1: {
                name: 'На Билайн России',
                value: '∞ мин',
                color: '#FF6688',
                width: '16.6667%',
                height: '100%'
            },
            2: {
                name: 'На все номера России',
                value: '1000 мин',
                color: '#992C2F',
                width: '16.6667%',
                height: '7.5%'
            },
            3: {
                name: 'По миру в 40 стран',
                value: '100 мин',
                color: '#7BA863',
                width: '16.6667%',
                height: '7.5%'
            },
            4: {
                name: 'Интернет',
                value: '10 ГБ',
                color: '#3395FF',
                width: '16.6667%',
                height: '25%'
            },
            5: {
                name: 'СМС',
                value: '∞',
                color: '#D9BC4C',
                width: '16.6667%',
                height: '100%'
            }
        },
    },
    ligth: {
        name: 'Лайт',
        blocks: {
            0: {
                name: 'Аб. плата',
                value: '990 руб',
                color: '#FF8463',
                width: '14.2857%',
                height: '38.2239%'
            },
            1: {
                name: 'На Билайн России',
                value: '∞ мин',
                color: '#FF6688',
                width: '14.2857%',
                height: '100%'
            },
            2: {
                name: 'На все номера России',
                value: '1500 мин',
                color: '#992C2F',
                width: '14.2857%',
                height: '25%'
            },
            3: {
                name: 'По миру в 40 стран',
                value: '150 мин',
                color: '#7BA863',
                width: '14.2857%',
                height: '25%'
            },
            4: {
                name: 'Входящие в популярных странах',
                value: '150 мин',
                color: '#F99F1E',
                width: '14.2857%',
                height: '2.5%'
            },
            5: {
                name: 'Интернет',
                value: '20 ГБ',
                color: '#3395FF',
                width: '14.2857%',
                height: '50%'
            },
            6: {
                name: 'СМС',
                value: '∞',
                color: '#D9BC4C',
                width: '14.2857%',
                height: '100%'
            }
        },
    },
    super: {
        name: 'Супер',
        blocks: {
            0: {
                name: 'Аб. плата',
                value: '1490 руб',
                color: '#FF8463',
                width: '14.2857%',
                height: '57.529%'
            },
            1: {
                name: 'На Билайн России',
                value: '∞ мин',
                color: '#FF6688',
                width: '14.2857%',
                height: '100%'
            },
            2: {
                name: 'На все номера России',
                value: '3000 мин',
                color: '#992C2F',
                width: '14.2857%',
                height: '50%'
            },
            3: {
                name: 'По миру в 40 стран',
                value: '300 мин',
                color: '#7BA863',
                width: '14.2857%',
                height: '50%'
            },
            4: {
                name: 'Вх. в Европе',
                value: '300 мин',
                color: '#F99F1E',
                width: '14.2857%',
                height: '50%'
            },
            5: {
                name: 'Интернет 4G ∞',
                value: '30 ГБ',
                color: '#3395FF',
                width: '14.2857%',
                height: '75%'
            },
            6: {
                name: 'СМС',
                value: '∞',
                color: '#D9BC4C',
                width: '14.2857%',
                height: '100%'
            }
        },
    },
    premium: {
        name: 'Премиум',
        blocks: {
            0: {
                name: 'Аб. плата',
                value: '2590 руб',
                color: '#FF8463',
                width: '12.5%',
                height: '100%'
            },
            1: {
                name: 'На Билайн России',
                value: '∞ мин',
                color: '#FF6688',
                width: '12.5%',
                height: '100%'
            },
            2: {
                name: 'На все номера России',
                value: '6000 мин',
                color: '#992C2F',
                width: '12.5%',
                height: '100%'
            },
            3: {
                name: 'По миру в 40 стран',
                value: '600 мин',
                color: '#7BA863',
                width: '12.5%',
                height: '100%'
            },
            4: {
                name: 'Исходящие в Европе в Россию',
                value: '1000 мин',
                color: '#EF7F1A',
                width: '12.5%',
                height: '100%'
            },
            5: {
                name: 'Вх. в Европе',
                value: '6000 мин',
                color: '#F99F1E',
                width: '12.5%',
                height: '100%'
            },
            6: {
                name: 'Интернет 4G ∞',
                value: '40 ГБ',
                color: '#3395FF',
                width: '12.5%',
                height: '100%'
            },
            7: {
                name: 'СМС',
                value: '∞',
                color: '#D9BC4C',
                width: '12.5%',
                height: '100%'
            }
        },
    }
}