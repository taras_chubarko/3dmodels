export default {
    intro: {
        name: 'Интро',
        prices: {
            activate: 0,
            balance: 0,
        },
        blocks: {
            beeline:{
                color:"#FF6688",
                max:9999,
                name:"На билайн России",
                txt:"&infin; мин",
                value:0,
            },
            minutes:{
                color:"#3395FF",
                max:30000,
                name:"Интернет",
                txt:"5 ГБ",
                value:0,
            },
            net:{
                color:"#FF8463",
                max:1890,
                name:"Аб. плата",
                txt:"390 руб",
                value:0,
            },
            numrf:{
                color:"#992C2F",
                max:6000,
                name:"На все номера России",
                txt:"800 мин",
                value:0,
            },
            sms:{
                color:"#D9BC4C",
                max:6000,
                name:"СМС",
                txt:"800 ШТ",
                value:0,
            }
        }
    }
}