export default function (Vue)
{
    Vue.block = {
        setBlock() {
            
        },
        getBlock(){
            return Vue.component('block-find', require('../components/blocks/Find.vue'));
        }
    }
    
    Object.defineProperties(Vue.prototype, {
        $block: {
            get() {
                return Vue.block
            }
        }
    });
}