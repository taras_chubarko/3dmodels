export default function (Vue)
{
    Vue.auth = {
        setToken(cartalyst_sentinel, user, expiration) {
            localStorage.setItem('cartalyst_sentinel', cartalyst_sentinel)
            localStorage.setItem('user', user)
            localStorage.setItem('expiration', expiration)
        },
        //
        getToken() {
            var cartalyst_sentinel = localStorage.getItem('cartalyst_sentinel');
            var expiration = localStorage.getItem('expiration');
            //
            if (!cartalyst_sentinel || !expiration ) {
                return null;
            }
            //
            var ts = Math.round((new Date()).getTime() / 1000);
            if (ts > parseInt(expiration)) {
                this.destroyToken();
                return null;
            }
            else
            {
                return cartalyst_sentinel;
            }
            
        },
        //
        destroyToken() {
            localStorage.removeItem('cartalyst_sentinel');
            localStorage.removeItem('user');
            localStorage.removeItem('expiration');
            //window.location.href = '/'; 
        },
        //
        isAuth() {
            if (this.getToken()) {
                return true;
            }
            else{
                return false;
            }
        },
        whatch() {
            var expiration = localStorage.getItem('expiration');
            var ts = Math.round((new Date()).getTime() / 1000);
            if (ts > parseInt(expiration)) {
                this.destroyToken();
                window.location.href = '/'; 
            }
        },
        getUser()
        {
            if (this.getToken()) {
                return JSON.parse(localStorage.getItem('user'));
            }
            else{
                return false;
            }
        },
        logout()
        {
            localStorage.removeItem('cartalyst_sentinel');
            localStorage.removeItem('user');
            localStorage.removeItem('expiration');
        },
        inRole(role)
        {
            var user = JSON.parse(localStorage.getItem('user'));
            var rol = false;
            if (user) {
                $.each(user.roles, function(k,v){
                    if (v.slug === role) {
                        rol = true;
                    }
                });
            }
            
            return rol;
        }
    }
    
    Object.defineProperties(Vue.prototype, {
        $auth: {
            get() {
                return Vue.auth
            }
        }
    });
}