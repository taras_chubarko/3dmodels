export default function (Vue)
{
    Vue.msg = {
        setErrors(errors, notify, title) {
            var arr = [];
            $.each(errors.all(), function(k,v){
                arr.push('<li>'+v+'</li>');
            });
            
            notify({
                type: 'error',
                title: title, 
                text: '<ul>'+arr.join("")+'</ul>',
            });
            
            $.each(errors.items, function(n,m){
                $('[name='+m.field+']').parent().addClass('has-error');
            });
            
            $('.has-error').on('click', function(){
                $(this).removeClass('has-error');
            });
        },
        err(errors, notify, title)
        {
            var arr = [];
            $.each(errors, function(k,v){
                arr.push('<li>'+v+'</li>');
            });
            
            notify({
                type: 'error',
                title: title, 
                text: '<ul>'+arr.join("")+'</ul>',
            });
        }
    }
    
    Object.defineProperties(Vue.prototype, {
        $msg: {
            get() {
                return Vue.msg
            }
        }
    });
}