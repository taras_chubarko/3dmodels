let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/themes/site/assets/js')
   .sass('resources/assets/sass/app.scss', 'public/themes/site/assets/css');
   
mix.js('resources/assets/js/adminApp.js', 'public/themes/admin/assets/js')
   .sass('resources/assets/sass/adminApp.scss', 'public/themes/admin/assets/css');
   
mix.options({
    processCssUrls: false
});
